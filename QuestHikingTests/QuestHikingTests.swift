//
//  QuestHikingTests.swift
//  QuestHikingTests
//
//  Created by Yury Bogdanov on 23.03.17.
//  Copyright © 2017 Yury Bogdanov. All rights reserved.
//

import XCTest


class QuestHikingTests: XCTestCase {
    let connector = ServiceConnector()
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testGettingQuestList() {
        connector.getQuestList {
            response, error in
            XCTAssert(response?.count != 0, "Server returned zero quests")
        }
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
