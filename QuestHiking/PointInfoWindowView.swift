//
//  PointInfoWindowView.swift
//  QuestHiking
//
//  Created by Yury Bogdanov on 24.02.17.
//  Copyright © 2017 Yury Bogdanov. All rights reserved.
//

import UIKit
import AlamofireImage

protocol PointInfoWindowDelegate {
    var gamePoints: [GamePoint]? { get set }
    func pointInfoCloseButton()
}

class PointInfoWindowView: UIView {
    let nibName = "QuestSimpleQuestionView"
    var view: UIView!
    var point: QuestPoint = QuestPoint()
    var delegate: PointInfoWindowDelegate? {
        didSet {
            setup()
        }
    }
    var pointsAcquired: Int? {
        didSet {
            setupAcquiredPoinsLabel()
            NotificationCenter.default.post(Notification.init(name: Notification.Name(rawValue: "userLoaded")))
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func loadFromNib() -> UIView  {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        
        view = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        return view
    }
    
    func setup() {
        view = loadFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    
    func setupSubviews() {
        setupTitle()
        setupImage()
        setupDescription()
        setupButton()
    }
    
    func setupTitle() {
        let titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.bounds.width, height: 44))
        titleLabel.text = "Верно!"
        titleLabel.textAlignment = .center
        titleLabel.font = UIFont.preferredFont(forTextStyle: .title3)
        titleLabel.roundLabel(corners: [.topLeft, .topRight], radius: 15)
        titleLabel.backgroundColor = QH_GreenColor.withAlphaComponent(0.5)
        view.addSubview(titleLabel)
        
        let pointNameLabel = UILabel(frame: CGRect(x: 0, y: 44, width: view.bounds.width, height: 44))
        pointNameLabel.textAlignment = .center
        pointNameLabel.font = UIFont.preferredFont(forTextStyle: .headline)
        pointNameLabel.numberOfLines = 2
        pointNameLabel.lineBreakMode = .byWordWrapping
        pointNameLabel.text = point.name
        view.addSubview(pointNameLabel)
    }
    
    func setupImage() {
        let pointImage = UIImageView(frame: CGRect(x: 0, y: 88, width: view.bounds.width, height: view.bounds.height/3))
        pointImage.af_setImage(withURL: point.image.getImageURL())
        print(point.image.getImageURL())
        pointImage.contentMode = .scaleAspectFit
        view.addSubview(pointImage)
    }
    
    func setupDescription() {
        let descriptonText = UITextView(frame: CGRect(x: 10, y: 88+5+view.bounds.height/3, width: view.bounds.width-20, height: view.bounds.height/3))
        descriptonText.text = point.description
        if descriptonText.text == "" {
            descriptonText.text = "No desc!"
        }
        descriptonText.isEditable = false
        descriptonText.backgroundColor = UIColor.clear
        view.addSubview(descriptonText)
    }
    
    func setupButton() {
        let closeButton = UIButton(frame: CGRect(x: 0, y: view.bounds.maxY-44, width: view.bounds.width, height: 44))
        closeButton.setTitle("OK", for: .normal)
        closeButton.setTitleColor(UIColor.white, for: .normal)
        closeButton.backgroundColor = QH_BlueColor
        closeButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        closeButton.roundButton(corners: [.bottomLeft, .bottomRight], radius: CGFloat(15))
        view.addSubview(closeButton)
    }
    
    func buttonPressed() {
        delegate?.pointInfoCloseButton()
    }
    
    func setupAcquiredPoinsLabel() {
        let pointsLabel = UILabel(frame: CGRect(x: 0, y: view.bounds.maxY-74, width: view.bounds.width, height: 30))
        pointsLabel.text = "+\(pointsAcquired!) баллов"
        pointsLabel.textAlignment = .center
        pointsLabel.font = UIFont.preferredFont(forTextStyle: .subheadline)
        view.addSubview(pointsLabel)
    }
    
    //func callCard() {  // removed in QIOS-68
    func callCard(for point: QuestPoint) {
        if self.superview != nil {
            print("Calling card")
            self.point = point
            self.frame = CGRect(x: (self.superview?.bounds.width)!/10, y: (self.superview?.bounds.maxY)!, width: ((self.superview?.bounds.width)!/10)*8, height: ((self.superview?.bounds.height)!/8)*6)
            setupSubviews()
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 2.0, initialSpringVelocity: 6.0, options: .curveEaseInOut, animations: {
                self.frame = CGRect(x: (self.superview?.bounds.width)!/10,
                                    y: (self.superview?.bounds.height)!/8,
                                    width: ((self.superview?.bounds.width)!/10)*8,
                                    height: ((self.superview?.bounds.height)!/8)*6)
            })
        }
    }
    
    fileprivate func removeCard(alsoRemoveFromSuperView: Bool, direction: DismissalDirection) {
        var destinationFrame = CGRect()
        
        switch direction {
        case .up: destinationFrame = CGRect(x: (self.superview?.bounds.width)!/10, y: (self.superview?.bounds.minY)!+self.bounds.height, width: ((self.superview?.bounds.width)!/10)*8, height: ((self.superview?.bounds.height)!/8)*6)
        case .down: destinationFrame = CGRect(x: (self.superview?.bounds.width)!/10, y: (self.superview?.bounds.maxY)!, width: ((self.superview?.bounds.width)!/10)*8, height: ((self.superview?.bounds.height)!/8)*6)
        case .right: destinationFrame = CGRect(x: (self.superview?.bounds.maxX)!, y: (self.superview?.bounds.height)!/8, width: ((self.superview?.bounds.width)!/10)*8, height: ((self.superview?.bounds.height)!/8)*6)
        case .left: destinationFrame = CGRect(x: (self.superview?.bounds.minX)!-self.bounds.width, y: (self.superview?.bounds.height)!/8, width: ((self.superview?.bounds.width)!/10)*8, height: ((self.superview?.bounds.height)!/8)*6)
        }
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 2.0, initialSpringVelocity: 6.0, options: .curveEaseInOut, animations: {
            self.frame = destinationFrame
        }, completion: {
            result in
            if alsoRemoveFromSuperView {
                self.removeFromSuperview()
            }
            
        })
    }
    
    func dismissCard() {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "cardDismissed"), object: nil)
        removeCard(alsoRemoveFromSuperView: true, direction: .down)
    }
    
    func hideCard() {
        removeCard(alsoRemoveFromSuperView: false, direction: .right)
    }

    
}
