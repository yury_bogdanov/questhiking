//
//  MainViewController.swift
//  QuestHiking
//
//  Created by Yury Bogdanov on 21.01.17.
//  Copyright © 2017 Yury Bogdanov. All rights reserved.
//

import UIKit
import SCLAlertView

let QH_BlueColor = UIColor(red: 0, green: 78/255.0, blue: 236/255.0, alpha: 1.0)
let QH_GreenColor = UIColor(red: 0, green: 199/255.0, blue: 0, alpha: 1.0)

class MainViewController: UIViewController, UIBarPositioningDelegate {
    @IBOutlet var mapViewController: UIView!
    @IBOutlet var tableViewController: UIView!
    @IBOutlet var viewSwitcher: UISegmentedControl!
    var questList: [Quest] = [Quest]()
    var selectedQuestForSegue: Quest = Quest()
    @IBOutlet var toolbar: UIToolbar!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        mapViewController.isHidden = true
        tableViewController.isHidden = false
        self.navigationItem.title = "Квестовый Поход"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        getQuests()
        print("Main Quest List: \(questList)")
        NotificationCenter.default.addObserver(self, selector: #selector(goToSelectedQuest(notification:)), name: NSNotification.Name(rawValue: "moveToSelectedQuest"), object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func viewSwitched(_ sender: Any) {
        if viewSwitcher.selectedSegmentIndex == 0 {
            self.mapViewController.isHidden = true
            self.tableViewController.isHidden = false
        }
        if viewSwitcher.selectedSegmentIndex == 1 {
            self.mapViewController.isHidden = false
            self.tableViewController.isHidden = true
        }
    }

    func getQuests() {
        ServiceConnector.sharedInstance.getQuestList {
            (response, error) in
            guard error == nil else {
                let appearance = SCLAlertView.SCLAppearance(showCloseButton: false)
                let alert = SCLAlertView(appearance: appearance)
                alert.addButton("OK", action: {
                    self.getQuests()
                })
                alert.showError("Ошибка", subTitle: (error?.domain)!)
                return
            }
            self.questList = response!
            NotificationCenter.default.post(name: NSNotification.Name("questsLoaded"), object: response!)
            self.displayLoadingErrorIfExists(error: error)
        }
    }
    
    func displayLoadingErrorIfExists(error: NSError?) {
        guard error == nil else {
            let errorView = SCLAlertView()
            let errorInfo = "\((error?.code)!): \((error?.domain)!)"
            errorView.showError("Ошибка", subTitle: errorInfo, closeButtonTitle: "OK")
            return
        }
    }
    
    func position(for bar: UIBarPositioning) -> UIBarPosition {
        return .top
    }
    
    // MARK: - Navigation

    func goToSelectedQuest(notification: Notification) {
        selectedQuestForSegue = notification.object as! Quest
        self.performSegue(withIdentifier: "showQuest", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "embeddedMapViewSegue" {
            let embeddedView = segue.destination as! QuestListMapViewController
            embeddedView.questList = questList
        }
        if segue.identifier == "embeddedTableViewSegue" {
            let embeddedView = segue.destination as! QuestListViewController
            embeddedView.questList = questList
        }
        if segue.identifier == "showQuest" {
            let destination = segue.destination as! QuestViewController
            destination.quest = selectedQuestForSegue
        }
    }

}
