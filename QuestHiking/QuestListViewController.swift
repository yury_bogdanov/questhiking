//
//  QuestListViewController.swift
//  QuestHiking
//
//  Created by Yury Bogdanov on 21.01.17.
//  Copyright © 2017 Yury Bogdanov. All rights reserved.
//

import UIKit
import SCLAlertView

class QuestListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet var tableView: UITableView!
    var questList: [Quest] = [Quest]()
    @IBOutlet var activityIndicator: UIActivityIndicatorView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        self.automaticallyAdjustsScrollViewInsets = false
        print("QUEST LIST: \(questList)")
        setupLoader()
        NotificationCenter.default.addObserver(self, selector: #selector(getQuests(from:)), name: NSNotification.Name(rawValue: "questsLoaded"), object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "questCell", for: indexPath) as! QuestCell
        if questList[indexPath.row].image?.getImageURL() == nil {
            print("some")
        } else {
            cell.questPhoto.af_setImage(withURL: (questList[indexPath.row].image?.getImageURL())!)
        }
        cell.questName.text = questList[indexPath.row].shortName
        cell.complexity = questList[indexPath.row].complexity
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //performSegue(withIdentifier: "showQuest", sender: self)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "moveToSelectedQuest"), object: questList[indexPath.row])
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func getQuests(from notification: NSNotification) {
        questList = notification.object as! [Quest]
        self.disableLoader()
        self.tableView.reloadData()
    }
    
    func displayLoadingErrorIfExists(error: NSError?) {
        guard error == nil else {
            let errorView = SCLAlertView()
            let errorInfo = "\((error?.code)!): \((error?.domain)!)"
            errorView.showError("Ошибка", subTitle: errorInfo, closeButtonTitle: "OK")
            return
        }
    }
    
    func setupLoader() {
        activityIndicator.startAnimating()
        activityIndicator.isHidden = false
    }
    
    func disableLoader() {
        activityIndicator.stopAnimating()
        activityIndicator.isHidden = true
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "showQuest" {
//            let destination = segue.destination as! QuestViewController
//            destination.quest = questList[(tableView.indexPathForSelectedRow?.row)!]
//        }
    }
    

}
