//
//  QuestQuestion.swift
//  QuestHiking
//
//  Created by Yury Bogdanov on 11.02.17.
//  Copyright © 2017 Yury Bogdanov. All rights reserved.
//

import Foundation
import SwiftyJSON

enum QuestionType {
    case simple, geo, none
}

class QuestQuestion {
    var id: Int = 0
    var questionContent: String = ""
    var questionType: QuestionType = .simple
    var answers: [QuestQuestionAnswer] = [QuestQuestionAnswer]()
    var hintCount: Int = 0
    
    init() {}
    
    init(with json: JSON!) {
        self.id = json["id"].intValue
        self.questionContent = json["questionContent"].stringValue
        self.hintCount = json["hintsCount"].intValue
        for json in json["answers"].array! {
            let answer = QuestQuestionAnswer(with: json)
            answers.append(answer)
        }
        switch json["questionType"].stringValue {
            case "SIMPLE": self.questionType = .simple
            case "GEO": self.questionType = .geo
            default: self.questionType = .none
        }
    }
}
