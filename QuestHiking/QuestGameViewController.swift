//
//  QuestGameViewController.swift
//  QuestHiking
//
//  Created by Yury Bogdanov on 05.02.17.
//  Copyright © 2017 Yury Bogdanov. All rights reserved.
//

import UIKit
import SCLAlertView
import VK_ios_sdk

enum CardType {
    case question, point
}

class QuestGameViewController: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate, QuestionViewDelegate, PointInfoWindowDelegate {
    @IBOutlet var mapView: GMSMapView!
    var infoBarView: PlayerBalancePanelView!
    var inPosButton: QuestButton!
    var questionButton: QuestButton!
    var questionCard: QuestSimpleQuestionView!
    var pointInfoCard: PointInfoWindowView!
    var locationManager = CLLocationManager()
    var didFinishMyLocation: Bool = false
    var quest = Quest()
    var game = QuestGame()
    var question: QuestQuestion? = QuestQuestion()
    var gamePoints: [GamePoint]? = [GamePoint]()
    var gameFinished: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupMaps()
        setupControls()
        drawInfoBar()
        print("QuestGameView.game.gameId = \(game.gameId)")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        drawGamePoints()
        self.tabBarController?.tabBar.isHidden = true
        mapView.addObserver(self, forKeyPath: "myLocation", options: .new, context: nil)
        setupNotificationSubscriptions()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        mapView.removeObserver(self, forKeyPath: "myLocation", context: nil)
        deleteNotificationSubscriptions()
    }
    
    func setupMaps() {
        let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(quest.startPoint.latitude), longitude: CLLocationDegrees(quest.startPoint.longitude), zoom: 16.0)
        mapView.camera = camera
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        setupStartPoint()
    }

    func setupPointMarker(with gamePoint: GamePoint) {
        let coordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(gamePoint.point.latitude), longitude: CLLocationDegrees(gamePoint.point.longitude))
        let locationMarker = GMSMarker(position: coordinate)
        locationMarker.map = mapView
        locationMarker.title = gamePoint.point.name
        locationMarker.snippet = gamePoint.point.description
        mapView.selectedMarker = locationMarker
        switch gamePoint.gamePointStatus {
        case .visited:
            locationMarker.icon = GMSMarker.markerImage(with: UIColor(red: 0, green: 0, blue: 195/255.0, alpha: 1.0))
        case .captured:
            locationMarker.icon = GMSMarker.markerImage(with: UIColor(red: 195/255.0, green: 0, blue: 0, alpha: 1.0))
        case .none:
            locationMarker.icon = GMSMarker.markerImage(with: UIColor(red: 0, green: 0, blue: 0, alpha: 1.0))
        }
    }
    
    func setupStartPoint() {
        let coordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(quest.startPoint.latitude), longitude: CLLocationDegrees(quest.startPoint.longitude))
        let locationMarker = GMSMarker(position: coordinate)
        locationMarker.map = mapView
        locationMarker.title = quest.startPoint.name
        locationMarker.snippet = quest.startPoint.description
    }
    
    func setupControls() {
        // Если кнопки не нарисованы - рисуем
        if inPosButton == nil {
            setupInPositionButton()
        }
        if questionButton == nil {
            setupQuestionButton()
        }
        inPosButton.isHidden = false
        questionButton.isHidden = false
        print("Game Points Count: \(gamePoints?.count)")
        if gamePoints?.count == 0 {
            questionButton.isHidden = true
            return
        }
        if gamePoints?.last?.gamePointStatus == .visited {
            infoBarView.status = .simpleQuestion
            inPosButton.isHidden = true
            return
        }
        if gamePoints?.last?.gamePointStatus == .captured {
            infoBarView.status = .geoQuestion
            return
        }
    }
    
    func setupInPositionButton() {
        inPosButton = QuestButton(frame: CGRect(x: 10,
                                                y: self.view.bounds.maxY-160,
                                                width: self.view.bounds.height/11,
                                                height: self.view.bounds.height/11), type: .checkLocation)
        inPosButton.addTarget(self, action: #selector(checkLocation), for: .touchUpInside)
        self.view.addSubview(inPosButton)
    }
    
    func setupQuestionButton() {
        questionButton = QuestButton(frame: CGRect(x: 10,
                                                   y: self.view.bounds.maxY-160-10-inPosButton.bounds.height,
                                                   width: self.view.bounds.height/11,
                                                   height: self.view.bounds.height/11), type: .showQuestion)
        questionButton.addTarget(self, action: #selector(getQuestion), for: .touchUpInside)
        self.view.addSubview(questionButton)
        
    }
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            mapView.isMyLocationEnabled = true
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if !didFinishMyLocation {
            let myLocation: CLLocation = change![NSKeyValueChangeKey.newKey] as! CLLocation
            mapView.settings.myLocationButton = true
        }
    }
    
    func drawGamePoints() {
        ServiceConnector.sharedInstance.game.getGamePoints(gameId: game.gameId) {
            response in
            print("GAME POINTS RESP: \(response.0)")
            self.gamePoints?.removeAll()
            response.0.forEach {
                self.setupPointMarker(with: $0)
                self.gamePoints?.append($0)
                self.setupControls()
            }
        }
    }

    func drawInfoBar() {
        self.navigationController?.navigationBar.isTranslucent = false
        infoBarView = PlayerBalancePanelView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 44))
        self.view.addSubview(infoBarView)
    }
    
    func setupNotificationSubscriptions() {
        NotificationCenter.default.addObserver(self, selector: #selector(getQuestion), name: Notification.Name(rawValue: "getGeoQuestion"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(checkAnswer(_:)), name: Notification.Name(rawValue: "answeredQuestion"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(checkLocation), name: Notification.Name(rawValue: "checkUserPosition"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(restoreGameButtons), name: Notification.Name(rawValue: "cardDismissed"), object: nil)
    }
    
    func deleteNotificationSubscriptions() {
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "getGeoQuestion"), object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "answeredQuestion"), object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "checkUserPosition"), object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "cardDismissed"), object: nil)
    }
    
    // MARK: - Game
    
    func callCard(of type: CardType, for point: QuestPoint?) {
        self.hideGameButtons()
        if type == .question {
            self.questionCard = QuestSimpleQuestionView(frame: CGRect(x: self.view.bounds.width/10,
                                                                     y: self.view.bounds.maxY,
                                                                     width: (self.view.bounds.width/10)*8,
                                                                     height: (self.view.bounds.height/8)*6))
            self.questionCard.delegate = self
            self.view.addSubview(self.questionCard)
            self.questionCard.callCard()
        } else if type == .point {
            self.pointInfoCard = PointInfoWindowView(frame: CGRect(x: self.view.bounds.width/10,
                                                                   y: self.view.bounds.maxY,
                                                                   width: (self.view.bounds.width/10)*8,
                                                                   height: (self.view.bounds.height/8)*6))
            self.pointInfoCard.delegate = self
            self.view.addSubview(self.pointInfoCard)
            if let showPoint = point {
                self.pointInfoCard.callCard(for: showPoint)
            }
        }
    }
    
    func hideGameButtons() {
        inPosButton.hideButton()
        questionButton.hideButton()
        self.setupControls()
    }
    
    func restoreGameButtons() {
        inPosButton.restoreButton()
        questionButton.restoreButton()
    }
    
    func getQuestion() {
        ServiceConnector.sharedInstance.game.getQuestion(gameId: game.gameId) {
            response in
            self.question = response.0
            self.performSegue(withIdentifier: "showQuestion", sender: self)
            //self.callCard(of: .question, for: nil) // Commented for QIOS-77
        }
    }
    
    func checkAnswer(_ notification: Notification) {
        let answer: String = notification.object as! String
        ServiceConnector.sharedInstance.game.checkAnswer(questionId: (question?.id)!, answer: answer, gameId: game.gameId) {
            response in
            switch response.0.isCorrect {
            case true:
                self.questionCard.dismissCard()
                if response.0.scoreObtained != 0 {
                    self.pointInfoCard.pointsAcquired = response.0.scoreObtained
                    self.infoBarView.updateInfo()
                }
                self.gameFinished = response.0.isGameFinished
                if self.gameFinished == false {
                    self.getQuestion()
                } else {
                    self.gameOver()
                }
                self.drawGamePoints()
                
                break
            case false:
                let alert = SCLAlertView()
                alert.showWarning("Неверно!", subTitle: "Вы дали неверный ответ", closeButtonTitle: "ОК")
                break
            }
        }
    }
    
    func pointInfoCloseButton() {
        self.pointInfoCard.dismissCard()
        //self.restoreGameButtons()  // QIOS-68 commented
        self.getQuestion()
    }
    
    func getCoordinates() -> (longitude: Double, latitude: Double) {
        let longitude = mapView.myLocation?.coordinate.longitude
        let latitude = mapView.myLocation?.coordinate.latitude
        if let long = longitude, let lat = latitude {
            return (long, lat)
        }
        return (0, 0)
    }
    
    func checkLocation() {
//        let longitude = mapView.myLocation?.coordinate.longitude
//        let latitude = mapView.myLocation?.coordinate.latitude
//        guard longitude != nil, latitude != nil else {
//            print("Longitude and latituder are nil")
//            return
//        }
        let coordinates = getCoordinates()
        self.hideGameButtons()
        ServiceConnector.sharedInstance.game.checkLocation(gameId: game.gameId, latitude: coordinates.latitude, longitude: coordinates.longitude) {
            response in
            print("RESPONSE: \(response)")
            switch response.0.isCorrect {
            case false:
                let appearance = SCLAlertView.SCLAppearance(showCloseButton: false)
                let alert = SCLAlertView(appearance: appearance)
                alert.addButton("OK", action: {
                    if self.questionCard == nil || self.questionCard.isOnScreen == false {
                        self.restoreGameButtons()
                    }
                })
                alert.showNotice("Результат", subTitle: "Неверное местоположение")
                break
            case true:
                if self.questionCard != nil && self.questionCard.isOnScreen {
                    self.questionCard.dismissCard()
                }
                self.callCard(of: .point, for: response.0.point) // Added for QIOS-64
                self.drawGamePoints()
                break
            }
        }
    }
    
    func getHint() {
        print("Requesting hint for game \(game.gameId)")
        guard self.question?.hintCount != 0 else {
            let alert = SCLAlertView()
            alert.showInfo("Подсказка", subTitle: "Нет подсказок для этого вопроса. \n Ищите сами.", closeButtonTitle: "OK")
            return
        }
        ServiceConnector.sharedInstance.game.getHint(for: game.gameId) {
            hint, error in
            let alert = SCLAlertView()
            var alertText = ""
            if hint.coinsObtained == 0 {
                alertText = "\(hint.hintCore.hintValue)"
            } else {
                alertText = "\(hint.hintCore.hintValue) \n Потрачено \(hint.coinsObtained) 💰"
            }
            alert.showInfo("Подсказка", subTitle: alertText, closeButtonTitle: "OK")
        }
    }
    
    func gameOver() {
        let appearance = SCLAlertView.SCLAppearance(showCloseButton: false)
        let gameOverAlert = SCLAlertView(appearance: appearance)
        if VKSdk.isLoggedIn() {
            gameOverAlert.addButton("Поделиться в VK", action: {
                print("Repost")
                ServiceConnector.sharedInstance.social.placeRepost(with: .gameOver, during: self.quest)
                self.performSegue(withIdentifier: "returnToQuestView", sender: self)
            })
        }
        gameOverAlert.addButton("OK", action: {
            self.performSegue(withIdentifier: "returnToQuestView", sender: self)
        })
        gameOverAlert.showSuccess("Игра завершена", subTitle: "Поздравляем! Вы прошли квест!")
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showQuestion" {
            let destination = segue.destination as! QuestionViewController
            destination.question = self.question
            destination.game = self.game
            destination.delegate = self
        }
        
    }

}
