//
//  QuestCheckAnswer.swift
//  QuestHiking
//
//  Created by Yury Bogdanov on 11.02.17.
//  Copyright © 2017 Yury Bogdanov. All rights reserved.
//

import Foundation
import SwiftyJSON

class QuestCheckAnswer {
    
    var coinsObtained: Int = 0
    var coinsTotal: Int = 0
    var isCorrect: Bool = false
    var isGameFinished: Bool = false
    var passPercentage: Int = 0
    var rank: String = ""
    var scoreObtained: Int = 0
    var scoreTotal: Int = 0
    
    init() {}
    
    init(with json: JSON) {
        scoreObtained = json["scoreObtained"].intValue
        coinsObtained = json["coinsObtained"].intValue
        coinsTotal = json["coinsTotal"].intValue
        scoreTotal = json["scoreTotal"].intValue
        rank = json["rank"].stringValue
        passPercentage = json["passPercentage"].intValue
        isCorrect = json["isCorrect"].boolValue
        isGameFinished = json["isGameFinished"].boolValue
    }
    
}
