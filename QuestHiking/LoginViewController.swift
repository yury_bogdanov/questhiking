//
//  LoginViewController.swift
//  QuestHiking
//
//  Created by Yury Bogdanov on 28.01.17.
//  Copyright © 2017 Yury Bogdanov. All rights reserved.
//

import UIKit
import SCLAlertView
import VK_ios_sdk
import SwiftyJSON
import CryptoSwift
import Firebase

class LoginViewController: UIViewController, VKSdkDelegate, VKSdkUIDelegate {
    @IBOutlet var loginField: UITextField!
    @IBOutlet var passwordField: UITextField!
    let sdkInstance = VKSdk.initialize(withAppId: "5742300")
    let concurrentQueue = DispatchQueue(label: "concurrent", attributes: .concurrent)
    var keyboardIsVisible = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTapToHide()
        setupVKSDK()
        //self.view.backgroundColor = UIColor(patternImage: UIImage(named: "stoleshnikov.jpg")!)
        
        NotificationCenter.default.addObserver(self, selector: #selector(moveToProfile), name: Notification.Name(rawValue: "tokenAcquired"), object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        guard UserDefaults.standard.value(forKey: quest_token) != nil else {
            let notificationCenter = NotificationCenter.default
            notificationCenter.addObserver(self, selector: #selector(handleKeyboardWillShowNotification(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
            notificationCenter.addObserver(self, selector: #selector(handleKeyboardWillHideNotification(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
            return
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        let notificationCenter = NotificationCenter.default
        notificationCenter.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        notificationCenter.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    func position(for bar: UIBarPositioning) -> UIBarPosition {
        return .topAttached
    }
    
    func setupTapToHide() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    @IBAction func signInTapped(_ sender: Any) {
        signIn(with: loginField.text!, password: passwordField.text!)
    }
    
    @IBAction func signInVKTapped(_ sender: Any) {
        VKSdk.authorize(["wall"], with: .unlimitedToken)
        print("Logged: \(VKSdk.isLoggedIn())")
    }
    
    func registerAndSignInUsingVK() {
        var id = String()
        var encodedId = Array(id.utf8)
        
        let req = VKApi.users().get(["fields":"photo_max_orig"])
        req?.execute(resultBlock: {response in
            let user = JSON(response?.json!)
            print("VK USER: \(user)")
            let name = "\(user[0]["first_name"].stringValue) \(user[0]["last_name"].stringValue)"
            id = "\(user[0]["id"].int!)"
            encodedId = Array(id.utf8)
            do {
                encodedId = try HMAC(key: "3540d67e045d4eebb8add8194aca2d2e", variant: .sha1).authenticate(encodedId)
                ServiceConnector.sharedInstance.registrationRequest(name: name, email: id, password: encodedId.toHexString(), usingVK: true) {
                    _ in
                }
            } catch {
                print("Error occured")
            }
            self.signIn(with: id, password: encodedId.toHexString())
        }, errorBlock: {error in
        })
    }
    
    @IBAction func registrationTapped(_ sender: Any) {
        self.performSegue(withIdentifier: "goToRegistration", sender: self)
    }
    
    func signIn(with username: String, password: String) {
        guard username != "", password != "" else {
            let alert = SCLAlertView()
            alert.showNotice("Ошибка", subTitle: "Введите логин и пароль и повторите попытку", closeButtonTitle: "ОК")
            return
        }
        ServiceConnector.sharedInstance.signIn(login: username, password: password)
    }
    
    // MARK: - VK SDK
    
    func setupVKSDK() {
        sdkInstance?.uiDelegate = self
        sdkInstance?.register(self)
        VKSdk.wakeUpSession(["wall"], complete: {
            state, error in
            if state == VKAuthorizationState.authorized {
                print("Authorized")
            } else {
                print("Not Authorized")
            }
        })
    }
    
    func vkSdkAccessAuthorizationFinished(with result: VKAuthorizationResult) {
        print("VK: \(result)")
        if VKSdk.isLoggedIn() {
            registerAndSignInUsingVK()
        }
    }
    
    func vkSdkUserAuthorizationFailed() {
        print("Authorization failed")
    }
    
    func vkSdkShouldPresent(_ controller: UIViewController!) {
        print("vkSdkShouldPresent")
        present(controller!, animated: true, completion: nil)
    }
    
    func vkSdkNeedCaptchaEnter(_ captchaError: VKError!) {
        print()
    }
    
    func handleKeyboardWillShowNotification(_ notification: Notification) {
        print("Keyboard up, \(keyboardIsVisible)")
        guard keyboardIsVisible == false else {
            return
        }
        print("After guard: Keyboard up, \(keyboardIsVisible)")
        keyboardWillChangeFrameWithNotification(notification, showsKeyboard: true)
        keyboardIsVisible = true
    }
    
    func handleKeyboardWillHideNotification(_ notification: Notification) {
        print("Keyboard down, \(keyboardIsVisible)")
        guard keyboardIsVisible == true else {
            return
        }
        keyboardWillChangeFrameWithNotification(notification, showsKeyboard: false)
        keyboardIsVisible = false
    }
    
    func keyboardWillChangeFrameWithNotification(_ notification: Notification, showsKeyboard: Bool) {
        
        let userInfo = (notification as NSNotification).userInfo!
        
        let animationDuration: TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        
        // Convert the keyboard frame from screen to view coordinates.
        let keyboardScreenBeginFrame = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        let keyboardViewBeginFrame = view.convert(keyboardScreenBeginFrame, from: view.window)
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        let originDelta = keyboardViewEndFrame.origin.y - keyboardViewBeginFrame.origin.y
        
        self.view.center.y += originDelta
        
        UIView.animate(withDuration: animationDuration, delay: 0, options: .beginFromCurrentState, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
 
    @IBAction func unwindToLoginScreen(segue: UIStoryboardSegue) {
        loginField.text = ""
        passwordField.text = ""
    }

    func moveToProfile() {
        self.dismiss(animated: true, completion: nil)
    }
}
