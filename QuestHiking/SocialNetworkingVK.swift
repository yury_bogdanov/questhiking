//
//  SocialNetworkingVK.swift
//  QuestHiking
//
//  Created by Yury Bogdanov on 11.03.17.
//  Copyright © 2017 Yury Bogdanov. All rights reserved.
//

import Foundation
import VK_ios_sdk

enum RepostType {
    case gameOver, paidRepost
}

class SocialNetworkingVK {
    
    func placeRepost(with type: RepostType, during quest: Quest) {
        var message = ""
        switch type {
        case .gameOver: message = "Я прошел квест \(quest.shortName) в приложении Квестовый Поход!"
            break
        case .paidRepost: message = "Можно вывезти девушку из деревни, но нельзя вывезти из девушки квест-монеты за репост"
            break
        }
        
        let req = VKApi.wall().post(["message": message, "photo": "photo-102997756_405830668", "attachments": "https://vk.com/kvestoviypohod"])
        req?.execute(resultBlock: {
            result in
            
        }, errorBlock: {error in})
    }
    
}
