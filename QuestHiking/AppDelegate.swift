//
//  AppDelegate.swift
//  QuestHiking
//
//  Created by Yury Bogdanov on 09.12.16.
//  Copyright © 2016 Yury Bogdanov. All rights reserved.
//

import UIKit
import VK_ios_sdk
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var shortcutItem: UIApplicationShortcutItem?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        var performShortcutDelegate = true
        if let shortcutItem = launchOptions?[.shortcutItem] as? UIApplicationShortcutItem {
            
            print("Application launched via shortcut")
            self.shortcutItem = shortcutItem
            
            performShortcutDelegate = false
        }
        
        VKSdk.initialize(withAppId: "5742300", apiVersion: "5.62")
        GMSServices.provideAPIKey("AIzaSyCfY9Y2GHkdRTP3GpIhAPlerIPthDLmVZs")
        if ServiceConnector.sharedInstance.isLoggedIn() == false {
            showLoginScreen()
        }
        VKSdk.wakeUpSession(["wall"], complete: {
            state, error in
            if state == VKAuthorizationState.authorized {
                print("Authorized")
            } else {
                print("Not Authorized")
            }
        })
        FIRApp.configure()
        return true
    }
    
    func applicationDidBecomeActive(application: UIApplication) {
        print("Application did become active")
        
        guard let shortcut = shortcutItem else { return }
        
        print("- Shortcut property has been set")
        
        handleQuickAction(shortcutItem: shortcut)
        
        self.shortcutItem = nil
        
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    // VK 
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        VKSdk.processOpen(url, fromApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String!)
        return true
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool
    {
        return VKSdk.processOpen(url, fromApplication: sourceApplication)
    }
    
    // Authorization
    func showLoginScreen() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let loginViewController = storyboard.instantiateViewController(withIdentifier: "loginScreen")
        //self.present(loginViewController, animated: true, completion: nil)
        self.window?.makeKeyAndVisible()
        self.window?.rootViewController?.present(loginViewController, animated: true, completion: nil)
    }
    
    // MARK: - Quick actions
    enum ShortcutType: String {
        case continueQuest = "Continue"
    }
    
    func handleQuickAction(shortcutItem: UIApplicationShortcutItem) -> Bool {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        var vc = UIViewController()
        var quickActionHandled: Bool = false
        let type = shortcutItem.type.components(separatedBy: ".").last!
        if let shortcutType = ShortcutType.init(rawValue: type) {
            switch shortcutType {
            case .continueQuest:
                vc = storyboard.instantiateViewController(withIdentifier: "profile") as! ProfileViewController
                quickActionHandled = true
                break
            default:
                break
            }
        }
        window?.rootViewController?.present(vc, animated: true, completion: nil)
        
        return quickActionHandled
    }
    
    func application(_ application: UIApplication, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
        completionHandler(handleQuickAction(shortcutItem: shortcutItem))
        
    }

}

