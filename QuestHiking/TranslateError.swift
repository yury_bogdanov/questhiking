//
//  TranslateError.swift
//  QuestHiking
//
//  Created by Yury Bogdanov on 08.01.17.
//  Copyright © 2017 Yury Bogdanov. All rights reserved.
//

import Foundation

enum ConnectorError {
    case ConnectionFailed
    case EmptyResponse
}

struct TranslateError {
    private var errorType: ConnectorError
    
    init(type: ConnectorError) {
        errorType = type
    }
    
    func getDomain() -> String {
        switch errorType {
        case .ConnectionFailed:
            return "Ошибка подключения. Попробуйте позже."
        case .EmptyResponse:
            return "В настоящее время доступных квестов нет. Попробуйте позже."
        }
    }
    
    func getCode() -> Int {
        switch errorType {
        case .ConnectionFailed:
            return 1009
        case .EmptyResponse:
            return 902
        }
    }
}
