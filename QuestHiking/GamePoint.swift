//
//  GamePoint.swift
//  
//
//  Created by Yury Bogdanov on 08.02.17.
//
//

import Foundation
import SwiftyJSON

enum GamePointStatus {
    case visited
    case captured
    case none
}

class GamePoint {
    var id: Int = 0
    var gamePointStatus: GamePointStatus = .none
    var point: QuestPoint = QuestPoint()
    
    init() {}
    
    init(with json: JSON!) {
        self.id = json["id"].intValue
        switch json["gamePointStatus"].stringValue {
        case "VISITED":
            self.gamePointStatus = .visited
        case "CAPTURED":
            self.gamePointStatus = .captured
        default:
            self.gamePointStatus = .none
        }
        self.point = QuestPoint(withJSON: json["point"])
    }
}
