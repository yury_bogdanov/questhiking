//
//  QuestListMapViewController.swift
//  QuestHiking
//
//  Created by Yury Bogdanov on 21.01.17.
//  Copyright © 2017 Yury Bogdanov. All rights reserved.
//

import UIKit
import SCLAlertView


class QuestListMapViewController: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate {
    @IBOutlet var mapView: GMSMapView!
    var questList: [Quest] = [Quest]()
    var locationManager = CLLocationManager()
    var didFinishMyLocation = false
    var questMarkers: [GMSMarker] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        self.automaticallyAdjustsScrollViewInsets = false
        setupMaps()
        NotificationCenter.default.addObserver(self, selector: #selector(getQuests(from:)), name: NSNotification.Name(rawValue: "questsLoaded"), object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getQuests(from notification: NSNotification) {
        questList = notification.object as! [Quest]
        setupQuestMarkers()
    }
    
    func displayLoadingErrorIfExists(error: NSError?) {
        guard error == nil else {
            let errorView = SCLAlertView()
            let errorInfo = "\((error?.code)!): \((error?.domain)!)"
            errorView.showError("Ошибка", subTitle: errorInfo, closeButtonTitle: "OK")
            return
        }
    }
    
    // MARK: - Google Maps Setup
    func setupMaps() {
        let camera = GMSCameraPosition.camera(withLatitude: 55.755833, longitude: 37.617778, zoom: 12.0)
        mapView.camera = camera
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        mapView.addObserver(self, forKeyPath: "myLocation", options: .new, context: nil)
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "moveToSelectedQuest"), object: marker.userData)
        //performSegue(withIdentifier: "showQuest", sender: marker.userData)
        print("infoWindow tapped")
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            mapView.isMyLocationEnabled = true
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if !didFinishMyLocation {
            let myLocation: CLLocation = change![NSKeyValueChangeKey.newKey] as! CLLocation
            mapView.settings.myLocationButton = true
        }
    }
    
    func setupQuestMarkers() {
        if questMarkers.count > 0 { questMarkers.removeAll() }
        for quest in questList {
            let questLongitude = quest.startPoint.longitude
            let questLatitude = quest.startPoint.latitude
            let coordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(questLatitude), longitude: CLLocationDegrees(questLongitude))
            let locationMarker = GMSMarker(position: coordinate)
            locationMarker.map = mapView
            locationMarker.title = quest.shortName
            locationMarker.snippet = quest.startPoint.name
            locationMarker.userData = quest.id
            locationMarker.title = quest.shortName
            locationMarker.snippet = quest.startPoint.name
            locationMarker.userData = quest
            locationMarker.appearAnimation = kGMSMarkerAnimationPop
            locationMarker.icon = GMSMarker.markerImage(with: UIColor(red: 0, green: 195/255.0, blue: 0, alpha: 1.0))
            locationMarker.isFlat = false
            questMarkers.append(locationMarker)
        }
    }

    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showQuestFromMap" {
            let destination = segue.destination as! QuestViewController
            destination.quest = sender as! Quest
        }
    }
 

}
