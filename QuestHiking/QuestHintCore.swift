//
//  QuestHintCore.swift
//  QuestHiking
//
//  Created by Yury Bogdanov on 08.03.17.
//  Copyright © 2017 Yury Bogdanov. All rights reserved.
//

import Foundation
import SwiftyJSON

class QuestHintCore {
    
    var hintId: Int = 0
    var hintValue: String = ""
    var hintOrder: Int = 0
    
    init() {}
    
    init(with json: JSON) {
        print("HINT CORE FULL: \(json)")
        hintId = json["id"].intValue
        hintValue = json["value"].stringValue
        hintOrder = json["order"].intValue
    }
    
}
