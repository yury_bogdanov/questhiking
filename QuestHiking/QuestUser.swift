//
//  QuestUser.swift
//  QuestHiking
//
//  Created by Yury Bogdanov on 01.02.17.
//  Copyright © 2017 Yury Bogdanov. All rights reserved.
//

import Foundation
import SwiftyJSON
import VK_ios_sdk

class QuestUser {
    var userId: String = ""
    var name: String = ""
    var email: String = ""
    var registrationDate: String = ""
    var player: QuestUserPlayer = QuestUserPlayer()
    var photoLink: String = ""
    var photo: UIImage = UIImage()
    var cropRect: CGRect = CGRect()
    
    init() {
    }
    
    init(with json: JSON!) {
        self.userId = json["id"].stringValue
        self.name = json["name"].stringValue
        self.email = json["email"].stringValue
        self.registrationDate = formatDate(date:  Date(timeIntervalSince1970: TimeInterval(json["registrationDate"].stringValue)!/1000))
        self.player = QuestUserPlayer(with: json["player"])
        getVKUserPhoto()
    }
    
    private func formatDate(date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        print(formatter.string(from: date))
        return formatter.string(from: date)
    }
    
    private func getVKUserPhoto() {
        let req = VKApi.users().get(["fields":"photo_100"])
        req?.execute(resultBlock: {response in
            let user = JSON(response?.json!)
            self.photoLink = user[0]["photo_100"].stringValue
            print("PHOTO LOADED: \(response)")
            NotificationCenter.default.post(name: NSNotification.Name("userLoaded"), object: response)
        }, errorBlock: {error in
        })

    }
}

extension Date {
    init(dateString: String) {
        let dateStringFormatter = DateFormatter()
        dateStringFormatter.dateFormat = "yyyy-MM-dd"
        dateStringFormatter.locale = Locale(identifier: "en_US_POSIX")
        let d = dateStringFormatter.date(from: dateString)!
        self.init(timeInterval:0, since:d)
    }
}

extension UIImage {
    func crop(image: UIImage, rect: CGRect) -> CGImage {
        let newImg = image.cgImage?.cropping(to: rect)
        return newImg!
    }
}
