//
//  PlayerBalancePanelView.swift
//  QuestHiking
//
//  Created by Yury Bogdanov on 25.02.17.
//  Copyright © 2017 Yury Bogdanov. All rights reserved.
//

import UIKit

enum PanelStatus {
    case gameStart, simpleQuestion, geoQuestion, gameOver, unknown
}

class PlayerBalancePanelView: UIView {

    let nibName = "PlayerBalancePanelView"
    var view: UIView!
    var balanceLabel: UILabel!
    var statusLabel: UILabel!
    var status: PanelStatus = .unknown {
        didSet {
            updateStatusLabelText()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    func loadFromNib() -> UIView  {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        
        view = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        return view
    }
    
    func setup() {
        view = loadFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        setupStatusLabel()
        setupBalanceLabel()
        addSubview(view)
    }
    
    func setupStatusLabel() {
        statusLabel = UILabel(frame: CGRect(x: 10, y: 0, width: self.view.bounds.width, height: self.view.bounds.height))
        updateStatusLabelText()
        statusLabel.font = UIFont.preferredFont(forTextStyle: .headline)
        statusLabel.textColor = UIColor.white
        self.view.addSubview(statusLabel)
    }
    
    func setupBalanceLabel() {
        balanceLabel = UILabel(frame: CGRect(x: 10, y: 0, width: view.bounds.width-20, height: 44))
        ServiceConnector.sharedInstance.getUserInfo {
            response in
            self.balanceLabel.text = "\((response.0?.player.gameScore)!) 🔶  \((response.0?.player.coins)!) 💰"
        }
        balanceLabel.textColor = UIColor.white
        balanceLabel.font = UIFont.preferredFont(forTextStyle: .headline)
        balanceLabel.textAlignment = .right
        view.addSubview(balanceLabel)
    }
    
    func updateStatusLabelText() {
        switch status {
        case .gameStart: statusLabel.text = "К стартовой точке"
        case .simpleQuestion: statusLabel.text = "Ответить на вопрос"
        case .geoQuestion: statusLabel.text = "К следующей точке"
        case .gameOver: statusLabel.text = "Игра завершена"
        default: statusLabel.text = "К стартовой точке"
        }
    }
    
    func updateInfo() {
        ServiceConnector.sharedInstance.getUserInfo {
            response in
            self.balanceLabel.text = "Баллы: \((response.0?.player.gameScore)!) Монеты: \((response.0?.player.coins)!)"
        }
    }
}
