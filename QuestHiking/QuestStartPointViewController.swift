//
//  QuestStartPointViewController.swift
//  QuestHiking
//
//  Created by Yury Bogdanov on 22.01.17.
//  Copyright © 2017 Yury Bogdanov. All rights reserved.
//

import UIKit

class QuestStartPointViewController: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate {
    @IBOutlet var mapView: GMSMapView!
    var point: QuestPoint = QuestPoint()
    var locationManager = CLLocationManager()
    var didFinishMyLocation: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        setupMaps()
        self.navigationItem.title = "Точка"
    }

    override func viewWillDisappear(_ animated: Bool) {
        mapView.removeObserver(self, forKeyPath: "myLocation", context: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupMaps() {
        let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(point.latitude), longitude: CLLocationDegrees(point.longitude), zoom: 16.0)
        mapView.camera = camera
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        mapView.addObserver(self, forKeyPath: "myLocation", options: .new, context: nil)
        setupPointMarker()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            mapView.isMyLocationEnabled = true
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if !didFinishMyLocation {
            let myLocation: CLLocation = change![NSKeyValueChangeKey.newKey] as! CLLocation
            mapView.settings.myLocationButton = true
        }
    }
    
    func setupPointMarker() {
        let coordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(point.latitude), longitude: CLLocationDegrees(point.longitude))
        let locationMarker = GMSMarker(position: coordinate)
        locationMarker.map = mapView
        locationMarker.title = point.name
        locationMarker.snippet = point.description
        mapView.selectedMarker = locationMarker
    }
    
    @IBAction func makeRoute(_ sender: Any) {
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
