//
//  QuestQuestionAnswer.swift
//  
//
//  Created by Yury Bogdanov on 11.02.17.
//
//

import Foundation
import SwiftyJSON

class QuestQuestionAnswer {
    var id: Int = 0
    var optionValue: String = ""
    
    init() {}
    
    init(with json: JSON!) {
        self.id = json["id"].intValue
        self.optionValue = json["optionValue"].stringValue
    }
    
    init(id: Int, value: String) {
        self.id = id
        self.optionValue = value
        
    }
}
