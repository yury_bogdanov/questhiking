//
//  QuestViewController.swift
//  QuestHiking
//
//  Created by Yury Bogdanov on 13.01.17.
//  Copyright © 2017 Yury Bogdanov. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import AlamofireImage
import SCLAlertView

class QuestViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet var tableView: UITableView!
    var questNameLabel: UILabel!
    var questId: Int = Int() // ID квеста, который необходимо отобразить
    var quest: Quest! = Quest()
    var game: QuestGame = QuestGame()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 44
        tableView.tableHeaderView = tableHeader()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.automaticallyAdjustsScrollViewInsets = false
        ServiceConnector.sharedInstance.getQuestInfo(forQuest: quest.id) {
            response, error in
            self.quest = response
            self.tableView.reloadData()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "complexityCell")
            cell?.textLabel?.text = "Сложность"
            switch quest.complexity {
                case .hard: cell?.detailTextLabel?.text = "HARD"
                case .low: cell?.detailTextLabel?.text = "LOW"
                case .medium: cell?.detailTextLabel?.text = "MEDIUM"
            }
            cell?.isUserInteractionEnabled = false
            return cell!
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "startPointCell")
            cell?.textLabel?.text = "Стартовая точка"
            cell?.detailTextLabel?.text = quest.startPoint.name
            return cell!
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "descriptionCell")
            cell?.textLabel?.text = quest.description
            cell?.isUserInteractionEnabled = false
            return cell!
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "controlsCell")
            // Начинаем квест
            return cell!
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "defaultCell")
            cell?.textLabel?.text = "Default cell."
            return cell!
        }
    }
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        if section == 0 {
//            let view = UIImageView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height/4))
//            view.af_setImage(withURL: (quest.image?.getImageURL())!)
//            view.contentMode = .scaleAspectFill
//            view.clipsToBounds = true
//            
//            let nameLabel: UILabel = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.width-20, height: view.bounds.height-20))
//            nameLabel.numberOfLines = 0
//            nameLabel.text = quest.shortName
//            nameLabel.textColor = UIColor.white
//            nameLabel.textAlignment = .center
//            nameLabel.font = UIFont.preferredFont(forTextStyle: .title1)
//            nameLabel.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.4)
//            nameLabel.clipsToBounds = true
//            nameLabel.layer.cornerRadius = 10
//            view.addSubview(nameLabel)
//            return view
//        }
//        return nil
//    }
    
    func tableHeader() -> UIView {
        let view = UIImageView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height/4))
        view.af_setImage(withURL: (quest.image?.getImageURL())!)
        view.contentMode = .scaleAspectFill
        view.clipsToBounds = true
        
        questNameLabel = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.bounds.width-20, height: view.bounds.height-20))
        questNameLabel.numberOfLines = 0
        questNameLabel.text = quest.shortName
        questNameLabel.textColor = UIColor.white
        questNameLabel.textAlignment = .center
        questNameLabel.font = UIFont.preferredFont(forTextStyle: .title1)
        questNameLabel.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.4)
        questNameLabel.clipsToBounds = true
        questNameLabel.layer.cornerRadius = 10
        view.addSubview(questNameLabel)
        return view
    }
    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        switch section {
//        case 0:
//            return self.view.bounds.height/4
//        default:
//            return 0
//        }
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 1 {
            performSegue(withIdentifier: "showStartPointInfo", sender: self)
        }
        if indexPath.row == 3 {
            ServiceConnector.sharedInstance.game.checkGameSession(forQuestId: self.quest.id) {
                game, passPercentage in
                print("GAME: \(game) PERCENTAGE: \(passPercentage)")
                if passPercentage == 0 {
                    self.startGame(for: self.quest.id)
                } else if passPercentage == 100 {
                    let alert = SCLAlertView()
                    alert.addButton("ОК", action: {
                        self.startGame(for: self.quest.id)
                    })
                    alert.showNotice("Квест уже пройден", subTitle: "Вы уже проходили этот квест, поэтому баллы начислены не будут", closeButtonTitle: "Отмена")
                } else {
                    let alert = SCLAlertView()
                    alert.addButton("Продолжить", action: {
                        self.game = game
                        self.performSegue(withIdentifier: "showGame", sender: self)
                    })
                    alert.addButton("Новая игра", action: {
                        self.startGame(for: self.quest.id)
                    })
                    alert.showNotice("Что делаем?", subTitle: "Вы уже начинали выполнять этот квест", closeButtonTitle: "Отмена")
                }
            }
            
        }
        if indexPath.row == 4 {
            if UserDefaults.standard.value(forKey: quest_token) == nil {
                let alert = SCLAlertView()
                alert.showWarning("Недоступно", subTitle: "Чтобы начать квест необходимо авторизоваться в разделе Профиль")
            }
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func startGame(for questId: Int) {
        ServiceConnector.sharedInstance.game.startGame(questId: self.quest.id) {
            response in
            self.game = QuestGame(id: response.0!)
            self.performSegue(withIdentifier: "showGame", sender: self)
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showStartPointInfo" {
            let destination = segue.destination as! QuestStartPointViewController
            destination.point = quest.startPoint
        }
        if segue.identifier == "showGame" {
            let destination = segue.destination as! QuestGameViewController
            destination.quest = quest
            destination.game = game
        }
    }
    
    @IBAction func unwindToQuestView(segue: UIStoryboardSegue) {}
    

}
