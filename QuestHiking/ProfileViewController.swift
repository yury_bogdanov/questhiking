//
//  ProfileViewController.swift
//  QuestHiking
//
//  Created by Yury Bogdanov on 29.01.17.
//  Copyright © 2017 Yury Bogdanov. All rights reserved.
//

import UIKit
import SCLAlertView
import VK_ios_sdk
import SwiftyJSON

class ProfileViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var currentUser: QuestUser = QuestUser()
    @IBOutlet var tableView: UITableView!
    let mainTableRows = ["Дата регистрации", "Баллы", "Монеты"]
    let concurrentQueue = DispatchQueue(label: "loadPortrait", attributes: .concurrent)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        self.navigationItem.title = "Профиль"
        self.automaticallyAdjustsScrollViewInsets = false
        self.navigationItem.hidesBackButton = true
        self.navigationController?.isNavigationBarHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(updateInterface), name: NSNotification.Name(rawValue: "userLoaded"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(getUser), name: NSNotification.Name(rawValue: "tokenAcquired"), object: nil)
        getUser()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    func getUser() {
        ServiceConnector.sharedInstance.getUserInfo {
            (response, error) in
            guard error == nil else {
                let alert = SCLAlertView()
                alert.showError("Ошибка", subTitle: (error?.description)!)
                return
            }
            self.currentUser = response!
            NotificationCenter.default.post(name: NSNotification.Name("userLoaded"), object: response)
        }
        tableView.reloadData()
    }
    
    func updateInterface() {
        tableView.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0: return mainTableRows.count
        case 1: return 1
        case 2: return 1
        default: return 0
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0: return currentUser.name
        default: return ""
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 && indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            cell.textLabel?.text = mainTableRows[indexPath.row]
            cell.detailTextLabel?.text = "\(currentUser.registrationDate)"
            cell.isUserInteractionEnabled = false
            return cell
        }
        if indexPath.section == 0 && indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            cell.textLabel?.text = mainTableRows[indexPath.row]
            cell.detailTextLabel?.text = "\(currentUser.player.gameScore) 🔶"
            cell.isUserInteractionEnabled = false
            return cell
        }
        if indexPath.section == 0 && indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            cell.textLabel?.text = mainTableRows[indexPath.row]
            cell.detailTextLabel?.text = "\(currentUser.player.coins) 💰"
            cell.isUserInteractionEnabled = false
            return cell
        }
        if indexPath.section == 1 && indexPath.row == 0 {
            let about = tableView.dequeueReusableCell(withIdentifier: "about", for: indexPath)
            return about
        }
        if indexPath.section == 2 && indexPath.row == 0 {
            let quit = tableView.dequeueReusableCell(withIdentifier: "quit", for: indexPath)
            return quit
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 2 && indexPath.row == 0 {
            let alert = SCLAlertView()
            alert.addButton("Выйти", action: {
                ServiceConnector.sharedInstance.logOff()
                self.showLoginScreen()
            })
            alert.showWarning("Подтвердить", subTitle: "Вы действительно хотите выйти?", closeButtonTitle: "Отмена")
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            self.tableView.register(UINib(nibName: "ProfileHeaderCell", bundle: nil), forCellReuseIdentifier: "profileHeaderCell")
            let view = tableView.dequeueReusableCell(withIdentifier: "profileHeaderCell") as! ProfileHeaderCell
            if currentUser.photoLink != "" {
                view.profilePhoto.af_setImage(withURL: URL(string: currentUser.photoLink)!)
            } else {
                view.profilePhoto.image = UIImage(named: "icon.png")
            }
            view.profilePhoto.layer.cornerRadius = view.profilePhoto.frame.size.width/2
            view.profilePhoto.clipsToBounds = true
            view.profilePhoto.layer.borderWidth = 3
            view.profilePhoto.layer.borderColor = UIColor.white.cgColor
            view.nameLabel.text = currentUser.name
            view.titleLabel.text = currentUser.player.rank
            VKSdk.wakeUpSession(["wall"], complete: {
                state, error in
                if state == VKAuthorizationState.authorized {
                    view.vkMark.isHidden = false
                } else {
                    view.vkMark.isHidden = true
                }
            })
            return view
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return 185
        default:
            return 0
        }
    }
    
    func showLoginScreen() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let loginViewController = storyboard.instantiateViewController(withIdentifier: "loginScreen")
        self.present(loginViewController, animated: true, completion: nil)
    }
    
//    func setupNavbar() {
//        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "navbar.png"), for: .default)
//        //self.navigationController?.navigationBar.barTintColor = QH_BlueColor
//        self.navigationController?.navigationBar.shadowImage = UIImage()
//        self.navigationController?.navigationBar.tintColor = UIColor.white
//        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
//        
//    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}

// MARK: - Extension
extension UIImage {
    func crop(rect: CGRect) -> UIImage {
        var rect = rect
        rect.origin.x *= self.scale
        rect.origin.y *= self.scale
        rect.size.width *= self.scale
        rect.size.height *= self.scale
        
        let imageReferrence: CGImage! = self.cgImage?.cropping(to: rect)
        let image = UIImage(cgImage: imageReferrence, scale: self.scale, orientation: self.imageOrientation)
        return image
    }
}
