//
//  Quest.swift
//  QuestHiking
//
//  Created by Yury Bogdanov on 21.01.17.
//  Copyright © 2017 Yury Bogdanov. All rights reserved.
//

import Foundation
import SwiftyJSON

enum QuestComplexity {
    case low
    case medium
    case hard
}

class Quest {
    var id: Int = 0
    var shortName: String = ""
    var description: String = ""
    var isActive: Bool = false
    var complexity: QuestComplexity
    var createDate: Date? = Date()
    var image: QuestImage? = QuestImage()
    var startPoint: QuestPoint = QuestPoint()
 
    init() {
        id = 0
        shortName = ""
        description = ""
        isActive = false
        complexity = .hard
        createDate = Date()
    }
    
    init(withJSON json: JSON!) {
        id = json["id"].intValue
        shortName = json["shortName"].stringValue
        description = json["description"].stringValue
        isActive = json["isActive"].boolValue
        switch json["complexity"].stringValue {
            case "LOW": complexity = .low
            case "MEDIUM": complexity = .medium
            case "HARD": complexity = .hard
        default: complexity = .low
        }
        createDate = date(from: json["createDate"].string)
        startPoint = QuestPoint(withJSON: json["startPoint"])
        image = QuestImage(withJSON: json["image"])
    }
    
    /**
     Преобразует дату из исходного JSON-а в нормальную дату
     */
    private func date(from string: String?) -> Date? {
        guard string != nil else {
            return nil
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        return dateFormatter.date(from: string!)
    }
    
}
