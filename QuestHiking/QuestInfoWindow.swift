//
//  QuestInfoWindow.swift
//  QuestHiking
//
//  Created by Yury Bogdanov on 19.01.17.
//  Copyright © 2017 Yury Bogdanov. All rights reserved.
//

import UIKit

class QuestInfoWindow: UIView {

    @IBOutlet var backgroundView: UIView!
    @IBOutlet var questTitle: UILabel!
    @IBOutlet var startPointTitle: UILabel!
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
//    override func draw(_ rect: CGRect) {
//    }

}
