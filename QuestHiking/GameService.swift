//
//  GameService.swift
//  QuestHiking
//
//  Created by Yury Bogdanov on 05.02.17.
//  Copyright © 2017 Yury Bogdanov. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class GameService {
    
    private let server = "http://46.101.181.17:8080/api/game/"
    
    /**
     Проверка игровых сессий
     */
    func checkGameSession(forQuestId: Int, complete: @escaping (_ game: QuestGame, _ passPercentage: Int) -> ()) {
        ServiceConnector.sharedInstance.getGameSessions {
            result in
            var runningSession: QuestGameSession? = nil
            for session in result.0 {
                if session.questId == forQuestId {
                    runningSession = session
                }
            }
            guard runningSession != nil else {
                self.startGame(questId: forQuestId) {
                    response in
                    complete(QuestGame(id: response.0!), 0)
                }
                return
            }
            complete(QuestGame(id: (runningSession?.gameId)!), (runningSession?.passPercentage)!)
        }
    }
    
    /**
     Начало игры
     */
    func startGame(questId: Int, complete: @escaping (_ response: Int?, _ error: NSError?) -> ()) {
        let url = server + "start"
        let parameters = ["questId": questId]
        let headers = ["Cookie": UserDefaults.standard.value(forKey: quest_token) as! String]
        
        Alamofire.request(url, method: .get, parameters: parameters, headers: headers).responseString {
            response in
            complete(Int(response.result.value!), nil)
        }
    }
    
    /** 
     Проверка локации
     */
    func checkLocation(gameId: Int, latitude: Double, longitude: Double, complete: @escaping (_ response: QuestCheckLocation, _ error: NSError?) -> ()) {
        let url = server + "check-location"
        let parameters: Parameters = ["latitude": latitude, "longitude": longitude, "gameId": gameId]
        let headers = ["Cookie": UserDefaults.standard.value(forKey: quest_token)! as! String]
        print(parameters)
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: headers).responseJSON {
            response in
            let json = JSON(rawValue: response.result.value!)
            print(json ?? "Location check JSON goes here")
            let result = QuestCheckLocation(with: json!)
            complete(result, nil)
        }
    }
    
    /**
     Проверка точек игры
     */
    func getGamePoints(gameId: Int, complete: @escaping (_ response: [GamePoint], _ error: NSError?) -> ()) {
        let url = server + "game-points"
        let parameters = ["gameId": gameId]
        let headers = ["Cookie": UserDefaults.standard.value(forKey: quest_token) as! String]
        
        Alamofire.request(url, method: .get, parameters: parameters, headers: headers).responseJSON {
            response in
            var gamePoints = [GamePoint]()
            print(response.result.value ?? "Game points JSON")
            for json in response.result.value as! [[String: Any]] {
                let point = JSON(json)
                gamePoints.append(GamePoint(with: point))
            }
            //print("GAME POINTS: \(gamePoints.first)")
            complete(gamePoints, nil)
        }
    }
    
    /**
     Получение вопроса
     */
    func getQuestion(gameId: Int, complete: @escaping (_ response: QuestQuestion, _ error: NSError?) -> ()) {
        let url = server + "question"
        let parameters: Parameters = ["gameId": gameId]
        let headers = ["Cookie": UserDefaults.standard.value(forKey: quest_token) as! String]
        
        Alamofire.request(url, method: .get, parameters: parameters, headers: headers).responseJSON {
            response in
            print("QUESTION: \(response)")
            let question = QuestQuestion(with: JSON(response.result.value as! [String: Any]))
            print("Question in GS: \(question.questionContent)")
            complete(question, nil)
        }
    }
    
    /**
     Проверка ответа
     */
    
    func checkAnswer(questionId: Int, answer: String, gameId: Int, complete: @escaping (_ response: QuestCheckAnswer, _ error: NSError?) -> ()) {
        let url = server + "check-answer"
        let parameters: Parameters = ["questionId": questionId, "answer": answer, "gameId": gameId]
        let headers = ["Cookie": UserDefaults.standard.value(forKey: quest_token) as! String]
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: headers).responseJSON {
            response in
            print(response)
            let json = JSON(rawValue: response.result.value!)
            print(json ?? "Answer check JSON")
            let result = QuestCheckAnswer(with: json!)
            complete(result, nil)
        }
    }
    
    /** 
     Получение подсказки
     */
    func getHint(for gameId: Int, complete: @escaping (_ response: QuestHint, _ error: NSError?) -> ()) {
        let url = server + "hint"
        let parameters: Parameters = ["gameId": gameId, "order": 1]
        let headers = ["Cookie": UserDefaults.standard.value(forKey: quest_token) as! String]
        
        let req = Alamofire.request(url, method: .get, parameters: parameters, headers: headers).responseJSON {
            response in
            print(response)
            let hint = QuestHint(with: JSON(response.result.value as! [String: Any]))
            print("VALUE: \(response.result.value as! [String: Any])")
            complete(hint, nil)
        }
    }
}
