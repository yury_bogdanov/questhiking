//
//  AnswerInputCell.swift
//  QuestHiking
//
//  Created by Yury Bogdanov on 04.04.17.
//  Copyright © 2017 Yury Bogdanov. All rights reserved.
//

import UIKit

class AnswerInputCell: UITableViewCell, UITextFieldDelegate {
    @IBOutlet var textField: UITextField!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
