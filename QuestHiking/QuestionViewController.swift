//
//  QuestionViewController.swift
//  QuestHiking
//
//  Created by Yury Bogdanov on 03.04.17.
//  Copyright © 2017 Yury Bogdanov. All rights reserved.
//

import UIKit
import SCLAlertView

class QuestionViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UINavigationBarDelegate {
    @IBOutlet var tableView: UITableView!
    @IBOutlet var navigationBar: UINavigationBar!
    @IBOutlet var answerField: UITextField!
    var question: QuestQuestion?
    var answerToSubmit: String = ""
    var game = QuestGame()
    var delegate: QuestGameViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationBar.delegate = self
        self.navigationBar.barTintColor = QH_BlueColor
        self.navigationBar.tintColor = UIColor.white
        self.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        self.navigationBar.roundBar(corners: [.topLeft, .topRight], radius: 10)
        //self.navigationBar.clipsToBounds = true
        tableView.delegate = self
        tableView.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancelPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            if let currentQuestion = question {
                if currentQuestion.questionType == .simple {
                    return "Вопрос"
                } else if currentQuestion.questionType == .geo {
                    return "Куда идти?"
                }
            }
            return ""
        case 1:
            if let currentQuestion = question {
                if currentQuestion.questionType == .geo {
                    return ""
                } else if currentQuestion.answers.count <= 1 {
                    return "Ввести ответ"
                } else if currentQuestion.answers.count > 1 {
                    return "Ответы"
                }
            }
            return ""
        default:
            return ""
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            if let currentQuestion = question {
                if currentQuestion.questionType == .geo {
                    return 1
                } else if currentQuestion.answers.count > 1 {
                    return currentQuestion.answers.count
                } else {
                    return 2
                }
            }
        case 2:
            return 2
        default:
            return 0
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let currentQuestion = question {
            if currentQuestion.questionType == .simple && currentQuestion.answers.count <= 1 && indexPath.section == 1 {
                return 44
            }
        }
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Ячейка вопроса
        if indexPath.section == 0 && indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "questionCell")
            cell?.textLabel?.text = question?.questionContent
            if let returnCell = cell {
                return returnCell
            }
        }
        // Ячейки ответов
        // Если question.answers.count <= 1, то показываем поле для ввода и кнопку Submit
        // Если question.answers.count > 1, то показываем варианты ответов
        if indexPath.section == 1 {
            if let currentQuestion = question {
                if currentQuestion.questionType == .geo && indexPath.row == 0 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "answerSubmitCell", for: indexPath)
                    cell.textLabel?.text = "Я на месте"
                    return cell
                } else if currentQuestion.answers.count > 1 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "answerCell")
                    cell?.textLabel?.text = currentQuestion.answers[indexPath.row].optionValue
                    if let returnCell = cell {
                        return returnCell
                    }
                } else if currentQuestion.answers.count <= 1 && indexPath.row == 0 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "answerInputCell", for: indexPath)
                    return cell
                } else if currentQuestion.answers.count <= 1 && indexPath.row == 1 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "answerSubmitCell", for: indexPath)
                    return cell
                }
            }

        }
        
        // Ячейки получения подсказок/покупки ответов
        if indexPath.section == 2 && indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "hintCell")
            cell?.textLabel?.text = "Подсказка"
            cell?.textLabel?.textColor = QH_BlueColor
            if let returnCell = cell {
                return returnCell
            }
        }
        if indexPath.section == 2 && indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "hintCell")
            cell?.textLabel?.text = "Купить ответ"
            cell?.textLabel?.textColor = QH_GreenColor
            if let returnCell = cell {
                return returnCell
            }
        }
        let cell = UITableViewCell()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let currentQuestion = question {
            if currentQuestion.questionType == .geo && indexPath.section == 1 {
                self.checkLocation()
            }
            if currentQuestion.answers.count <= 1 && indexPath.row == 1 {
                let index = IndexPath(row: 0, section: 1)
                let cell: AnswerInputCell = tableView.cellForRow(at: index) as! AnswerInputCell
                if let answer = cell.textField.text {
                    self.check(answer: answer)
                }
            }
            if currentQuestion.answers.count > 1 && indexPath.section == 1 {
                let cell = tableView.cellForRow(at: indexPath)
                if let answer = cell?.textLabel?.text {
                    self.check(answer: answer)
                }
                
            }
            if indexPath.section == 2 && indexPath.row == 0 {
                self.getHint()
            }
        }
        
    }
    
    // MARK: - Game
    func check(answer: String) {
        ServiceConnector.sharedInstance.game.checkAnswer(questionId: (question?.id)!, answer: answer, gameId: game.gameId) {
            response in
            switch response.0.isCorrect {
            case true:
                self.dismiss(animated: true, completion: nil)
                if response.0.scoreObtained != 0 {
                    self.delegate?.infoBarView.updateInfo()
                }
                self.delegate?.gameFinished = response.0.isGameFinished
                if self.delegate?.gameFinished == false {
                    self.delegate?.getQuestion()
                } else {
                    self.delegate?.gameOver()
                }
                self.delegate?.drawGamePoints()
                
                break
            case false:
                let alert = SCLAlertView()
                alert.showWarning("Неверно!", subTitle: "Вы дали неверный ответ", closeButtonTitle: "ОК")
                break
            }
        }
    }
    
    func checkLocation() {
        if let deleg = delegate {
            let coordinates = deleg.getCoordinates()
            ServiceConnector.sharedInstance.game.checkLocation(gameId: game.gameId, latitude: coordinates.latitude, longitude: coordinates.longitude) {
                response in
                print("RESPONSE: \(response)")
                switch response.0.isCorrect {
                case false:
                    let alert = SCLAlertView()
                    alert.showNotice("Результат", subTitle: "Неверное местоположение", closeButtonTitle: "OK")
                    break
                case true:
                    self.dismiss(animated: true, completion: {
                        deleg.callCard(of: .point, for: response.0.point)
                    })
                    break
                }
            }
        }
    }
    
    func getHint() {
        print("Requesting hint for game \(game.gameId)")
        if let currentQuestion = question {
            print("Hints count for question is: \(currentQuestion.hintCount)")
            if currentQuestion.hintCount > 0 {
                ServiceConnector.sharedInstance.game.getHint(for: game.gameId) {
                    hint, error in
                    let alert = SCLAlertView()
                    var alertText = ""
                    if hint.coinsObtained == 0 {
                        alertText = "\(hint.hintCore.hintValue)"
                    } else {
                        alertText = "\(hint.hintCore.hintValue) \n Потрачено \(hint.coinsObtained) 💰"
                    }
                    alert.showInfo("Подсказка", subTitle: alertText, closeButtonTitle: "OK")
                }
            } else {
                let alert = SCLAlertView()
                alert.showNotice("Нет подсказок", subTitle: "Подсказок для этого вопроса нет")
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
