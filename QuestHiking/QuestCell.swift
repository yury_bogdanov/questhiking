//
//  QuestCell.swift
//  QuestHiking
//
//  Created by Yury Bogdanov on 05.01.17.
//  Copyright © 2017 Yury Bogdanov. All rights reserved.
//

import UIKit
import SwiftyJSON
import AlamofireImage

class QuestCell: UITableViewCell {
    @IBOutlet var questName: UILabel!
    @IBOutlet var questComplexity: UILabel!
    var complexity: QuestComplexity = .low
    @IBOutlet var questPhoto: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()

        switch complexity {
        case .hard: questComplexity.text = "HARD"
        case .medium: questComplexity.text = "MEDIUM"
        case .low: questComplexity.text = "LOW"
        default:
            questComplexity.text = "UNDEF"
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
