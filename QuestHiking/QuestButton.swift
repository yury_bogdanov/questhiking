//
//  QuestButton.swift
//  QuestHiking
//
//  Created by Yury Bogdanov on 05.03.17.
//  Copyright © 2017 Yury Bogdanov. All rights reserved.
//

import UIKit

enum QuestButtonType {
    case checkLocation, showQuestion
}

class QuestButton: UIButton {

    var questButtonType: QuestButtonType
    var initialFrame: CGRect
    
    required init(frame: CGRect, type: QuestButtonType) {
        questButtonType = type
        initialFrame = frame
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setup() {
        switch questButtonType {
        case .checkLocation:
            let buttonImage = UIImage(named: "checkLocation")
            self.setImage(buttonImage, for: .normal)
            self.setImage(UIImage(named: "checkLocationPressed"), for: .highlighted)
            break
        case .showQuestion:
            let buttonImage = UIImage(named: "showQuestion")
            self.setImage(buttonImage, for: .normal)
            break
        }
    }
    
    func hideButton() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 2.0, initialSpringVelocity: 6.0, options: .curveEaseInOut, animations: {
            self.frame = CGRect(x: (self.superview?.bounds.minX)!-self.initialFrame.size.width, y: self.initialFrame.origin.y, width: self.initialFrame.size.width, height: self.initialFrame.size.height)
        }, completion: nil)
    }
    
    func restoreButton() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 2.0, initialSpringVelocity: 6.0, options: .curveEaseInOut, animations: {
            self.frame = self.initialFrame
        }, completion: nil)
    }
    
}
