//
//  Extensions.swift
//  QuestHiking
//
//  Created by Yury Bogdanov on 04.03.17.
//  Copyright © 2017 Yury Bogdanov. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    func roundButton(corners: UIRectCorner, radius: CGFloat) {
        let maskPath1 = UIBezierPath(roundedRect: self.bounds,
                                     byRoundingCorners: corners,
                                     cornerRadii:CGSize(width: radius, height: radius))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = self.bounds
        maskLayer1.path = maskPath1.cgPath
        self.layer.mask = maskLayer1
    }
}

extension UILabel {
    func roundLabel(corners: UIRectCorner, radius: CGFloat) {
        let maskPath1 = UIBezierPath(roundedRect: self.bounds,
                                     byRoundingCorners: corners,
                                     cornerRadii:CGSize(width: radius, height: radius))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = self.bounds
        maskLayer1.path = maskPath1.cgPath
        self.layer.mask = maskLayer1
    }
}

extension UINavigationBar {
    func roundBar(corners: UIRectCorner, radius: CGFloat) {
        if let sView = self.superview {
            let maskPath1 = UIBezierPath(roundedRect: sView.bounds,
                                         byRoundingCorners: corners,
                                         cornerRadii:CGSize(width: radius, height: radius))
            let maskLayer1 = CAShapeLayer()
            maskLayer1.frame = sView.bounds
            maskLayer1.path = maskPath1.cgPath
            self.layer.mask = maskLayer1
        }
    }
}
