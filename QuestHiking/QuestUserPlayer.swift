//
//  QuestUserPlayer.swift
//  QuestHiking
//
//  Created by Yury Bogdanov on 01.02.17.
//  Copyright © 2017 Yury Bogdanov. All rights reserved.
//

import Foundation
import SwiftyJSON

class QuestUserPlayer {
    var gameScore: Int = 0
    var rank: String = ""
    var coins: Int = 0
    var madeRepost: Bool = false
    
    init() {
    }
    
    init(score: Int, rank: String, coins: Int, madeRepost: Bool) {
        self.gameScore = score
        self.rank = rank
        self.coins = coins
        self.madeRepost = madeRepost
    }
    
    init(with json: JSON!) {
        self.gameScore = json["gameScore"].intValue
        self.rank = json["rank"].stringValue
        self.coins = json["coins"].intValue
        self.madeRepost = json["madeRepost"].boolValue
    }
    
}
