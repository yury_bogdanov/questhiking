//
//  QuestGame.swift
//  QuestHiking
//
//  Created by Yury Bogdanov on 05.02.17.
//  Copyright © 2017 Yury Bogdanov. All rights reserved.
//

import Foundation

class QuestGame {
    var gameId: Int = 0
    
    init() {}
    
    init(id: Int) {
        self.gameId = id
    }
}
