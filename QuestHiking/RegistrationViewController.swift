//
//  RegistrationViewController.swift
//  QuestHiking
//
//  Created by Yury Bogdanov on 29.01.17.
//  Copyright © 2017 Yury Bogdanov. All rights reserved.
//

import UIKit
import SCLAlertView

class RegistrationViewController: UIViewController {
    var keyboardIsVisible = false
    @IBOutlet var emailField: UITextField!
    @IBOutlet var passwordField: UITextField!
    @IBOutlet var passwordRepeatField: UITextField!
    @IBOutlet var nameField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backToLogin(_ sender: Any) {
        self.performSegue(withIdentifier: "unwindToLogin", sender: self)
    }
    
    func setupTapToHide() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(handleKeyboardWillShowNotification(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        notificationCenter.addObserver(self, selector: #selector(handleKeyboardWillHideNotification(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func handleKeyboardWillShowNotification(_ notification: Notification) {
        print("Keyboard up, \(keyboardIsVisible)")
        guard keyboardIsVisible == false else {
            return
        }
        print("After guard: Keyboard up, \(keyboardIsVisible)")
        keyboardWillChangeFrameWithNotification(notification, showsKeyboard: true)
        keyboardIsVisible = true
    }
    
    func handleKeyboardWillHideNotification(_ notification: Notification) {
        print("Keyboard down, \(keyboardIsVisible)")
        guard keyboardIsVisible == true else {
            return
        }
        keyboardWillChangeFrameWithNotification(notification, showsKeyboard: false)
        keyboardIsVisible = false
    }
    
    func keyboardWillChangeFrameWithNotification(_ notification: Notification, showsKeyboard: Bool) {
        
        let userInfo = (notification as NSNotification).userInfo!
        
        let animationDuration: TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        
        // Convert the keyboard frame from screen to view coordinates.
        let keyboardScreenBeginFrame = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        let keyboardViewBeginFrame = view.convert(keyboardScreenBeginFrame, from: view.window)
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        let originDelta = keyboardViewEndFrame.origin.y - keyboardViewBeginFrame.origin.y
        
        self.view.center.y += originDelta
        
        UIView.animate(withDuration: animationDuration, delay: 0, options: .beginFromCurrentState, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }

    @IBAction func registerUserTapped(_ sender: Any) {
        guard nameField.text != "",
              passwordField.text != "",
              passwordRepeatField.text != "",
              emailField.text != "" else {
                let alert = SCLAlertView()
                alert.showNotice("Ошибка", subTitle: "Заполните все поля")
                return
        }
        guard passwordRepeatField.text == passwordField.text else {
            let alert = SCLAlertView()
            alert.showNotice("Ошибка", subTitle: "Пароли должны совпадать")
            return
        }
        ServiceConnector.sharedInstance.registrationRequest(name: nameField.text, email: emailField.text, password: passwordField.text, usingVK: false) {
            result, error in
            guard result == true else {
                return
            }
            self.performSegue(withIdentifier: "unwindToLogin", sender: self)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
