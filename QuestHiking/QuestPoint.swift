//
//  QuestPoint.swift
//  QuestHiking
//
//  Created by Yury Bogdanov on 21.01.17.
//  Copyright © 2017 Yury Bogdanov. All rights reserved.
//

import Foundation
import SwiftyJSON

class QuestPoint {
    var id: Int = 0
    var latitude: Float = 0.0
    var longitude: Float = 0.0
    var name: String = ""
    var description: String = ""
    var order: Int = 0
    var startPoint: Bool = false
    var image: QuestImage = QuestImage()
    
    init() {
    }
    
    init(withJSON json: JSON!) {
        id = json["id"].intValue
        latitude = json["latitude"].floatValue
        longitude = json["longitude"].floatValue
        name = json["name"].stringValue
        description = json["description"].stringValue
        order = json["order"].intValue
        startPoint = json["startPoint"].boolValue
        image = QuestImage(withJSON: json["image"])
    }
    
}
