//
//  QuestSimpleQuestionView.swift
//  QuestHiking
//
//  Created by Yury Bogdanov on 13.02.17.
//  Copyright © 2017 Yury Bogdanov. All rights reserved.
//

import Foundation
import UIKit
import SCLAlertView

protocol QuestionViewDelegate {
    var question: QuestQuestion? { get set }
    
    func getHint()
}

enum DismissalDirection {
    case up, down, right, left
}

class QuestSimpleQuestionView: UIView {
    let nibName = "QuestSimpleQuestionView"
    var view: UIView!
    var hintButton: UIButton!
    var buyAnswerButton: UIButton!
    var answerButton: UIButton! // Показываем, если блок answers в ответе на запрос getQuestion пустой
    var textLabel: UILabel!
    var titleLabel: UILabel!
    var isOnScreen: Bool = false
    var delegate: QuestionViewDelegate? {
        didSet {
            setup()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    func loadFromNib() -> UIView  {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        
        view = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        return view
    }
    
    func setup() {
        view = loadFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    
    func setupSubviews() {
        setupButtons()
        setupQuestionText()
        setupGestures()
        if let answerCount = delegate?.question?.answers.count {
            if answerCount <= 1 {
                setupAnswerButton()
            } else {
                setupMultipleAnswerButtons()
            }
        }

    }
    
    func setupButtons() {
        hintButton = UIButton(frame: CGRect(x: 0,
                                            y: view.bounds.maxY-44,
                                        width: view.bounds.width/2,
                                       height: 44))
        hintButton.setTitle("Подсказка", for: .normal)
        hintButton.titleLabel?.textColor = UIColor.white
        hintButton.backgroundColor = UIColor(red: 0, green: 78/255.0, blue: 236/255.0, alpha: 1.0)
        hintButton.addTarget(self, action: #selector(hintButtonPressed), for: .touchUpInside)
        hintButton.roundButton(corners: .bottomLeft, radius: CGFloat(15))
        view.addSubview(hintButton)
        
        buyAnswerButton = UIButton(frame: CGRect(x: hintButton.bounds.maxX,
                                            y: view.bounds.maxY-44,
                                            width: view.bounds.width/2,
                                            height: 44))
        buyAnswerButton.setTitle("Купить ответ", for: .normal)
        buyAnswerButton.titleLabel?.textColor = UIColor.white
        buyAnswerButton.backgroundColor = UIColor(red: 0, green: 199/255.0, blue: 0, alpha: 1.0)
        buyAnswerButton.addTarget(self, action: #selector(buyAnswerButtonPressed), for: .touchUpInside)
        buyAnswerButton.roundButton(corners: .bottomRight, radius: CGFloat(15))
        view.addSubview(buyAnswerButton)
    }
    
    func setupQuestionText() {
        textLabel = UILabel(frame: CGRect(x: 8,
                                          y: 50,
                                      width: view.bounds.width - 16,
                                     height: 21))
        textLabel.numberOfLines = 0
        textLabel.lineBreakMode = .byWordWrapping
        textLabel.text = delegate?.question?.questionContent
        textLabel.sizeToFit()
        view.addSubview(textLabel)
        
        titleLabel = UILabel(frame: CGRect(x: 8,
                                           y: 8,
                                       width: view.bounds.width - 16,
                                      height: 34))
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .center
        if delegate?.question?.questionType == .simple {
            titleLabel.text = "Вопрос"
        } else {
            titleLabel.text = "Следующая точка"
        }
        titleLabel.font = UIFont.preferredFont(forTextStyle: .title1)
        view.addSubview(titleLabel)
    }
    
    func setupGestures() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissCard))
        self.view.addGestureRecognizer(tapGestureRecognizer)
    }
    
    func setupAnswerButton() {
        answerButton = UIButton(frame: CGRect(x: view.bounds.width/10,
                                              y: view.bounds.height/7 * 5,
                                          width: view.bounds.width/5 * 4,
                                         height: 50))
        answerButton.setTitleColor(QH_BlueColor, for: .normal)
        answerButton.setTitleColor(UIColor.black, for: .highlighted)
        answerButton.tag = 0
        answerButton.layer.borderWidth = 0.5
        answerButton.layer.borderColor = QH_BlueColor.cgColor
        answerButton.layer.cornerRadius = 10
        if delegate?.question?.questionType == .simple {
            answerButton.setTitle("Ответить", for: .normal)
            answerButton.addTarget(self, action: #selector(answerButtonTapped(_:)), for: .touchUpInside)
        } else {
            answerButton.setTitle("Я на месте", for: .normal)
            answerButton.addTarget(self, action: #selector(checkPosition), for: .touchUpInside)
        }
        view.addSubview(answerButton)
    }
    
    func setupMultipleAnswerButtons() {
        let offset = CGFloat(5)
        let buttonHeihgt = CGFloat(40)
        let buttonWidth = (view.bounds.width - 3 * offset) / 2
        let beginningHeight = view.bounds.height / 3 * 2 - 10
        let secondHeight = beginningHeight + buttonHeihgt + 5
        
        let button1 = UIButton(frame: CGRect(x: offset, y: beginningHeight, width: buttonWidth, height: buttonHeihgt))
        button1.setTitle(delegate?.question?.answers[0].optionValue, for: .normal)
        button1.setTitleColor(UIColor(red: 0, green: 78/255.0, blue: 236/255.0, alpha: 1.0), for: .normal)
        button1.titleLabel?.font = UIFont.preferredFont(forTextStyle: .subheadline)
        button1.titleLabel?.textAlignment = .center
        button1.titleLabel?.numberOfLines = 2
        button1.titleLabel?.lineBreakMode = .byWordWrapping
        button1.layer.borderWidth = 0.5
        button1.layer.borderColor = QH_BlueColor.cgColor
        button1.layer.cornerRadius = 5
        button1.tag = 1
        button1.addTarget(self, action: #selector(answerButtonTapped(_:)), for: .touchUpInside)
        view.addSubview(button1)
        
        let button2 = UIButton(frame: CGRect(x: buttonWidth+2*offset, y: beginningHeight, width: buttonWidth, height: buttonHeihgt))
        button2.setTitle(delegate?.question?.answers[1].optionValue, for: .normal)
        button2.setTitleColor(UIColor(red: 0, green: 78/255.0, blue: 236/255.0, alpha: 1.0), for: .normal)
        button2.titleLabel?.font = UIFont.preferredFont(forTextStyle: .subheadline)
        button2.titleLabel?.textAlignment = .center
        button2.titleLabel?.numberOfLines = 2
        button2.titleLabel?.lineBreakMode = .byWordWrapping
        button2.layer.borderWidth = 0.5
        button2.layer.borderColor = QH_BlueColor.cgColor
        button2.layer.cornerRadius = 5
        button2.tag = 2
        button2.addTarget(self, action: #selector(answerButtonTapped(_:)), for: .touchUpInside)
        view.addSubview(button2)
        
        let button3 = UIButton(frame: CGRect(x: offset, y: secondHeight, width: buttonWidth, height: buttonHeihgt))
        button3.setTitle(delegate?.question?.answers[2].optionValue, for: .normal)
        button3.setTitleColor(UIColor(red: 0, green: 78/255.0, blue: 236/255.0, alpha: 1.0), for: .normal)
        button3.titleLabel?.font = UIFont.preferredFont(forTextStyle: .subheadline)
        button3.titleLabel?.textAlignment = .center
        button3.titleLabel?.numberOfLines = 2
        button3.titleLabel?.lineBreakMode = .byWordWrapping
        button3.layer.borderWidth = 0.5
        button3.layer.borderColor = QH_BlueColor.cgColor
        button3.layer.cornerRadius = 5
        button3.tag = 3
        button3.addTarget(self, action: #selector(answerButtonTapped(_:)), for: .touchUpInside)
        view.addSubview(button3)
        
        let button4 = UIButton(frame: CGRect(x: buttonWidth+2*offset, y: secondHeight, width: buttonWidth, height: buttonHeihgt))
        button4.setTitle(delegate?.question?.answers[3].optionValue, for: .normal)
        button4.setTitleColor(UIColor(red: 0, green: 78/255.0, blue: 236/255.0, alpha: 1.0), for: .normal)
        button4.titleLabel?.font = UIFont.preferredFont(forTextStyle: .subheadline)
        button4.titleLabel?.textAlignment = .center
        button4.titleLabel?.numberOfLines = 2
        button4.titleLabel?.lineBreakMode = .byWordWrapping
        button4.layer.borderWidth = 0.5
        button4.layer.borderColor = QH_BlueColor.cgColor
        button4.layer.cornerRadius = 5
        button4.tag = 4
        button4.addTarget(self, action: #selector(answerButtonTapped(_:)), for: .touchUpInside)
        view.addSubview(button4)
        
    }
    
    func answerButtonTapped(_ button: UIButton) {
        switch button.tag {
        case 0:
            print("Question with no variants")
            let alert = SCLAlertView()
            let answer = alert.addTextField("Ответ")
            alert.addButton("OK", action: {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "answeredQuestion"), object: answer.text)
            })
            alert.showNotice("Ответ на вопрос", subTitle: "", closeButtonTitle: "Отмена")
            return
        case 1:
            print("First var")
            NotificationCenter.default.post(name: Notification.Name(rawValue: "answeredQuestion"), object: delegate?.question?.answers[button.tag-1].optionValue)
            return
        case 2:
            print("Second var")
            NotificationCenter.default.post(name: Notification.Name(rawValue: "answeredQuestion"), object: delegate?.question?.answers[button.tag-1].optionValue)
            return
        case 3:
            print("Third var")
            NotificationCenter.default.post(name: Notification.Name(rawValue: "answeredQuestion"), object: delegate?.question?.answers[button.tag-1].optionValue)
            return
        case 4:
            print("Fourth var")
            NotificationCenter.default.post(name: Notification.Name(rawValue: "answeredQuestion"), object: delegate?.question?.answers[button.tag-1].optionValue)
            return
        default:
            return
        }
    }
    
    func checkPosition() {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "checkUserPosition"), object: nil)
    }
    
    func hintButtonPressed() {
        print("Hint Button Pressed!")
        delegate?.getHint()
    }
    
    func buyAnswerButtonPressed() {
        print("Answer Button Pressed!")
    }
    
    func callCard() {
        if self.isOnScreen == false {
            print("Calling card")
            self.frame = CGRect(x: (self.superview?.bounds.width)!/10, y: (self.superview?.bounds.maxY)!, width: ((self.superview?.bounds.width)!/10)*8, height: ((self.superview?.bounds.height)!/8)*6)
            setupSubviews()
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 2.0, initialSpringVelocity: 6.0, options: .curveEaseInOut, animations: {
                    self.frame = CGRect(x: (self.superview?.bounds.width)!/10,
                                                      y: (self.superview?.bounds.height)!/8,
                                                      width: ((self.superview?.bounds.width)!/10)*8,
                                                      height: ((self.superview?.bounds.height)!/8)*6)
            }, completion: {
                result in
                self.isOnScreen = true
            })
        } else {
            dismissCard()
            callCard()
        }
    }
    
    fileprivate func removeCard(alsoRemoveFromSuperView: Bool, direction: DismissalDirection) {
        var destinationFrame = CGRect()
        
        switch direction {
        case .up: destinationFrame = CGRect(x: (self.superview?.bounds.width)!/10, y: (self.superview?.bounds.minY)!+self.bounds.height, width: ((self.superview?.bounds.width)!/10)*8, height: ((self.superview?.bounds.height)!/8)*6)
        case .down: destinationFrame = CGRect(x: (self.superview?.bounds.width)!/10, y: (self.superview?.bounds.maxY)!, width: ((self.superview?.bounds.width)!/10)*8, height: ((self.superview?.bounds.height)!/8)*6)
        case .right: destinationFrame = CGRect(x: (self.superview?.bounds.maxX)!, y: (self.superview?.bounds.height)!/8, width: ((self.superview?.bounds.width)!/10)*8, height: ((self.superview?.bounds.height)!/8)*6)
        case .left: destinationFrame = CGRect(x: (self.superview?.bounds.minX)!-self.bounds.width, y: (self.superview?.bounds.height)!/8, width: ((self.superview?.bounds.width)!/10)*8, height: ((self.superview?.bounds.height)!/8)*6)
        }
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 2.0, initialSpringVelocity: 6.0, options: .curveEaseInOut, animations: {
            self.frame = destinationFrame
        }, completion: {
            result in
            self.isOnScreen = false
            if alsoRemoveFromSuperView {
                self.removeFromSuperview()
            }
            
        })
    }
    
    func dismissCard() {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "cardDismissed"), object: nil)
        removeCard(alsoRemoveFromSuperView: true, direction: .down)
    }
    
    func hideCard() {
        removeCard(alsoRemoveFromSuperView: false, direction: .up)
    }
    
}
