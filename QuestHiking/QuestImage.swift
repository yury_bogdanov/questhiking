//
//  QuestImage.swift
//  QuestHiking
//
//  Created by Yury Bogdanov on 21.01.17.
//  Copyright © 2017 Yury Bogdanov. All rights reserved.
//

import Foundation
import SwiftyJSON
import AlamofireImage

class QuestImage {
    var id: Int = 0
    private var reference: String = ""
    var height: Int = 0
    var width: Int = 0
    var ext: String = ""
    var imgSize: Int = 0
    var prettyName: String = ""
    var imgDescription: String = ""
    
    init() {
    }
    
    init(withJSON json: JSON!) {
        id = json["id"].intValue
        reference = json["reference"].stringValue
        if json.isEmpty {
            reference = "http://mototochka.com/resources/images/design/no-image-large.jpg"
        }
        height = json["height"].intValue
        width = json["width"].intValue
        imgSize = json["size"].intValue
        ext = json["ext"].stringValue
        prettyName = json["prettyName"].stringValue
        imgDescription = json["description"].stringValue
    }
    
    func getImageURL() -> URL! {
        let url = URL(string: reference)
        return url
    }
}
