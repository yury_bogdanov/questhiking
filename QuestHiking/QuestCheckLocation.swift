//
//  QuestCheckLocation.swift
//  QuestHiking
//
//  Created by Yury Bogdanov on 05.02.17.
//  Copyright © 2017 Yury Bogdanov. All rights reserved.
//

import Foundation
import SwiftyJSON

class QuestCheckLocation {
    
    var scoreObtained: Int = 0
    var coinsObtained: Int = 0
    var coinsTotal: Int = 0
    var scoreTotal: Int = 0
    var rank: String = ""
    var passPercentage: Int = 0
    var isCorrect: Bool = false
    var point: QuestPoint = QuestPoint()
    
    init() {}
    
    init(with json: JSON) {
        scoreObtained = json["scoreObtained"].intValue
        coinsObtained = json["coinsObtained"].intValue
        coinsTotal = json["coinsTotal"].intValue
        scoreTotal = json["scoreTotal"].intValue
        rank = json["rank"].stringValue
        passPercentage = json["passPercentage"].intValue
        isCorrect = json["isCorrect"].boolValue
        point = QuestPoint(withJSON: json["point"])
    }
    
}
