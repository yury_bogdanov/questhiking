//
//  QuestHint.swift
//  QuestHiking
//
//  Created by Yury Bogdanov on 08.03.17.
//  Copyright © 2017 Yury Bogdanov. All rights reserved.
//

import Foundation
import SwiftyJSON

class QuestHint {
    
    var coinsObtained: Int = 0
    var coinsTotal: Int = 0
    var passPercentage: Int = 0
    var rank: String = ""
    var scoreObtained: Int = 0
    var scoreTotal: Int = 0
    var hintCore: QuestHintCore = QuestHintCore()
    
    init() {}
    
    init(with json: JSON) {
        scoreObtained = json["scoreObtained"].intValue
        coinsObtained = json["coinsObtained"].intValue
        coinsTotal = json["coinsTotal"].intValue
        scoreTotal = json["scoreTotal"].intValue
        rank = json["rank"].stringValue
        passPercentage = json["passPercentage"].intValue
        hintCore = QuestHintCore(with: json["hint"])
        print("HINT CORE: \(hintCore.hintValue)")
//        hintId = json["hint"]["hintId"].intValue
//        hintValue = json["hint"]["hintValue"].stringValue
//        hintOrder = json["hint"]["hintOrder"].intValue
    }
    
}
