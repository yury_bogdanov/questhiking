//
//  ServiceConnector.swift
//  QuestHiking
//
//  Created by Yury Bogdanov on 25.12.16.
//  Copyright © 2016 Yury Bogdanov. All rights reserved.
//

import Foundation
import Alamofire
import SCLAlertView
import SwiftyJSON
import VK_ios_sdk
import Firebase

private let _sharedInstance = ServiceConnector()
let quest_token = "QUEST_TOKEN"
let hasCookie = "HAS_COOKIE"

class ServiceConnector {
    class var sharedInstance: ServiceConnector {
        return _sharedInstance
    }
    private let server = "http://46.101.181.17:8080/api/"
    let game: GameService = GameService()
    let social: SocialNetworkingVK = SocialNetworkingVK()
    
    func registrationRequest(name: String!, email: String!, password: String!, usingVK: Bool, complete: @escaping (_ response: Bool, _ error: NSError?) -> ()) {
        var registrationURL = "http://46.101.181.17:8080/api/registration/regular"
        let parameters: Parameters = ["name": name, "email": email, "password": password]
        
        if usingVK {
            registrationURL = "http://46.101.181.17:8080/api/registration/vk"
        }
        
        let headers = ["Content-Type": "application/json"]
        
        let req = Alamofire.request(registrationURL, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseString {
            response in
            print("REGISTRATION: \(response.response)")
            if response.description == "SUCCESS: " && usingVK == false {
                SCLAlertView().showInfo("Успешно", subTitle: "Регистрация прошла успешно. Войдите в приложение.")
                complete(true, nil)
            } else if (response.description.contains("errorMessage")) {
                complete(false, nil)
                SCLAlertView().showError("Ошибка", subTitle: response.description)
            }
        }
        print(req)
        print(parameters)
    }
    
    func signIn(login: String, password: String)  {
        let loginURL = server + "login"
        let headers = ["Content-Type": "application/x-www-form-urlencoded"]
        let parameters = ["username":login, "password":password]
        FIRCrashMessage("Login parameters: \(parameters)")
        
        let req = Alamofire.request(loginURL, method: .post, parameters: parameters, headers: headers).response {
            response in
            let fullCookie: String! = response.response?.allHeaderFields["Set-Cookie"] as! String!
            print("COOKIE: \(response)")
            let start = fullCookie.startIndex
            let end = fullCookie.index(fullCookie.endIndex, offsetBy: -19)
            let range = start..<end
            let cookie = fullCookie.substring(with: range)
            UserDefaults.standard.set(true, forKey: hasCookie)
            // ToDo: Replace storing in UserDefaults with storing in Keychain
            UserDefaults.standard.set(cookie, forKey: quest_token)
            NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "tokenAcquired")))
            print("COOKIE END: \(cookie)")
        }
        print(req)

    }
    
    func getQuestList(complete: @escaping (_ response: [Quest]?, _ error: NSError?) -> ()) {
        let questListURL = server + "quest/all"
        let headers = ["Content-Type": "application/json"]
        
        Alamofire.request(questListURL, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON {
            response in
            
            print("GET QUEST LIST RESP: \(response.result.isFailure)")
            if response.result.isFailure {
                complete(nil, NSError(domain: "Проблемы с подключением. Попробуйте позже.", code: TranslateError(type: .ConnectionFailed).getCode(), userInfo: nil))
                return
            }
            if response.result.value == nil {
                complete(nil, NSError(domain: response.description, code: TranslateError(type: .EmptyResponse).getCode(), userInfo: nil))
                return
            }
            
            let json = JSON(rawValue: response.result.value!)
            var questList = [Quest]()
            for element in json! {
                let quest = Quest(withJSON: element.1)
                questList.append(quest)
            }
            complete(questList, nil)
        }
    }
    
    func getQuestInfo(forQuest id: Int, complete: @escaping (_ response: Quest?, _ error: NSError?) -> ()) {
        let questInfoURL = server + "quest/\(id)"
        let headers = ["Content-Type": "application/json"]
        
        Alamofire.request(questInfoURL, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON {
            response in
            print(response)
            if response.result.isFailure {
                complete(nil, NSError(domain: TranslateError(type: .ConnectionFailed).getDomain(), code: TranslateError(type: .ConnectionFailed).getCode(), userInfo: nil))
                return
            }
            if response.result.value == nil {
                complete(nil, NSError(domain: TranslateError(type: .EmptyResponse).getDomain(), code: TranslateError(type: .EmptyResponse).getCode(), userInfo: nil))
                return
            }
            
            let json = JSON(rawValue: response.result.value!)
            let quest = Quest(withJSON: json)
            complete(quest, nil)

        }
    }
    
    func getUserInfo(complete: @escaping (_ response: QuestUser?, _ error: NSError?) -> ()) {
        let userInfoURL = server + "user"
        print("TOKEN: \(UserDefaults.standard.value(forKey: quest_token))")
        let headers: HTTPHeaders = ["Content-Type": "application/json", "Cookie": UserDefaults.standard.value(forKey: quest_token) as! String]
    
        Alamofire.request(userInfoURL, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON {
            response in
            print("GET USER INFO RESPONSE: \(response)")
            guard response.result.value != nil else {
                complete(nil, NSError(domain: "Something went wrong", code: 401, userInfo: nil))
                return
            }
            let userJson: JSON! = JSON(rawValue: response.result.value!)
            let user = QuestUser(with: userJson)
            print("USER INFO: \(userJson)")
            complete(user, nil)
        }
    }
    
    /**
     Проверка игровых сессий пользователя
     */
    func getGameSessions(complete: @escaping (_ response: [QuestGameSession], _ error: NSError?) -> ()) {
        let url = server + "quest/games-by-user"
        let headers = ["Content-Type": "application/json", "Cookie": UserDefaults.standard.value(forKey: quest_token) as! String]
        
        Alamofire.request(url, method: .get, parameters: nil, headers: headers).responseJSON {
            response in
            
            var sessions = [QuestGameSession]()
            print("JSON: \(response.result.value)")
            for json in response.result.value as! [[String: Any]] {
                let session = JSON(json)
                sessions.append(QuestGameSession(with: session))
            }
            
            complete(sessions, nil)
            
        }
    }
    
    func logOff() {
        UserDefaults.standard.removeObject(forKey: quest_token)
        UserDefaults.standard.setValue(false, forKey: hasCookie)
        VKSdk.forceLogout()
    }
    
    func isLoggedIn() -> Bool {
        guard UserDefaults.standard.value(forKey: quest_token) != nil else {
            return false
        }
        return true
    }
    
}
