//
//  QuestGameSession.swift
//  QuestHiking
//
//  Created by Yury Bogdanov on 05.02.17.
//  Copyright © 2017 Yury Bogdanov. All rights reserved.
//

import Foundation
import SwiftyJSON

class QuestGameSession {
    var gameId: Int = 0
    var isFirstTime: Bool = false
    var passPercentage: Int = 0
    var questId: Int = 0
    var finishDate: String? = ""
    
    init() {
    }
    
    init(with json: JSON) {
        gameId = json["gameId"].intValue
        isFirstTime = json["isFirstTime"].boolValue
        passPercentage = json["passPercentage"].intValue
        questId = json["questId"].intValue
        guard json["finishDate"].string != nil else {
            return
        }
        finishDate = formatDate(date:  Date(timeIntervalSince1970: TimeInterval(json["finishDate"].string!)!/1000))
        
    }
    
    private func formatDate(date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        print(formatter.string(from: date))
        return formatter.string(from: date)
    }
}
